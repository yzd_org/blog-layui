# My-Blog-layui

**技术** 
- **Springboot**
- **Mybatis-plus**
- **mysql**
- **LayUI**
- **amaze**
- **javsScript**
- **ajax**
- **themleaf**
- **X-admin**
- **MEditor**
- **qcr**

## 部署
1，修改文件保存位置  
2，设置thymeleaf 缓存  
3，修改文件上传大小  
4，初始化系统qcr 服务器url  
5，取消注释登录邮件通知
6，数据源监控密码账号配置
7，邮件配置
8，百度统计
9，接口文档

Tencent docker 部署
docker ps
docker cp /data/docker/tomcat/blog.war 6c18e80deb67:/usr/local/tomcat/webapps/


## 作者

- 阳沐之的邮箱：17882555101@163.com

## 感谢
- [sentsin](https://github.com/sentsin/layui)
- [pandao](https://github.com/pandao/editor.md)