/*
 Navicat Premium Data Transfer

 Source Server         : yzd
 Source Server Type    : MySQL
 Source Server Version : 50705
 Source Host           : 122.51.85.120:3306
 Source Schema         : my_blog_db

 Target Server Type    : MySQL
 Target Server Version : 50705
 File Encoding         : 65001

 Date: 11/05/2020 17:24:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin_user`;
CREATE TABLE `tb_admin_user`  (
  `admin_user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `login_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员登陆名称',
  `login_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员登陆密码',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员显示昵称',
  `email` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `locked` tinyint(4) NULL DEFAULT 0 COMMENT '是否锁定 0未锁定 1已锁定无法登陆',
  PRIMARY KEY (`admin_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 175 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客系统管理员用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_admin_user
-- ----------------------------
INSERT INTO `tb_admin_user` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '阳沐之', '17882555101@qq.com', '2020-03-30 10:56:39', '2020-03-30 10:56:42', 0);
INSERT INTO `tb_admin_user` VALUES (2, 'guest', 'e10adc3949ba59abbe56e057f20f883e', 'guest', '121665820@qq.com', '2020-03-30 11:29:53', '2020-03-30 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (92, '邹峻', 'e10adc3949ba59abbe56e057f20f883e', '邹峻', '121665820@qq.com', '2020-03-30 11:29:53', '2020-03-30 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (93, '李先生', 'e10adc3949ba59abbe56e057f20f883e', '李先生', '121665821@qq.com', '2020-03-31 11:29:53', '2020-03-31 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (94, '张家铭', 'e10adc3949ba59abbe56e057f20f883e', '张家铭', '121665822@qq.com', '2020-04-01 11:29:53', '2020-04-01 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (95, '先红秀', 'e10adc3949ba59abbe56e057f20f883e', '先红秀', '121665823@qq.com', '2020-04-02 11:29:53', '2020-04-02 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (96, '阮斗才', 'e10adc3949ba59abbe56e057f20f883e', '阮斗才', '121665824@qq.com', '2020-04-03 11:29:53', '2020-04-03 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (97, '陶海江', 'e10adc3949ba59abbe56e057f20f883e', '陶海江', '121665825@qq.com', '2020-04-04 11:29:53', '2020-04-04 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (98, '王祖鑫', 'e10adc3949ba59abbe56e057f20f883e', '王祖鑫', '121665826@qq.com', '2020-04-05 11:29:53', '2020-04-05 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (99, '刘林昀', 'e10adc3949ba59abbe56e057f20f883e', '刘林昀', '121665827@qq.com', '2020-04-06 11:29:53', '2020-04-06 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (100, '舒楠', 'e10adc3949ba59abbe56e057f20f883e', '舒楠', '121665828@qq.com', '2020-04-07 11:29:53', '2020-04-07 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (101, '银丽', 'e10adc3949ba59abbe56e057f20f883e', '银丽', '121665829@qq.com', '2020-04-08 11:29:53', '2020-04-08 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (102, '杨葵', 'e10adc3949ba59abbe56e057f20f883e', '杨葵', '121665830@qq.com', '2020-04-09 11:29:53', '2020-04-09 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (103, '刘玲', 'e10adc3949ba59abbe56e057f20f883e', '刘玲', '121665831@qq.com', '2020-04-10 11:29:53', '2020-04-10 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (104, '罗雪', 'e10adc3949ba59abbe56e057f20f883e', '罗雪', '121665832@qq.com', '2020-04-11 11:29:53', '2020-04-11 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (105, '孙珺', 'e10adc3949ba59abbe56e057f20f883e', '孙珺', '121665833@qq.com', '2020-04-12 11:29:53', '2020-04-12 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (106, '方文静', 'e10adc3949ba59abbe56e057f20f883e', '方文静', '121665834@qq.com', '2020-04-13 11:29:53', '2020-04-13 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (107, '阳宗德', 'e10adc3949ba59abbe56e057f20f883e', '阳宗德', '121665835@qq.com', '2020-04-14 11:29:53', '2020-04-14 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (108, '彭超', 'e10adc3949ba59abbe56e057f20f883e', '彭超', '121665836@qq.com', '2020-04-15 11:29:53', '2020-04-15 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (109, '孙斌', 'e10adc3949ba59abbe56e057f20f883e', '孙斌', '121665837@qq.com', '2020-04-16 11:29:53', '2020-04-16 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (110, '王佳飞', 'e10adc3949ba59abbe56e057f20f883e', '王佳飞', '121665838@qq.com', '2020-04-17 11:29:53', '2020-04-17 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (111, '邹佳芯', 'e10adc3949ba59abbe56e057f20f883e', '邹佳芯', '121665839@qq.com', '2020-04-18 11:29:53', '2020-04-18 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (112, '刘敏', 'e10adc3949ba59abbe56e057f20f883e', '刘敏', '121665840@qq.com', '2020-04-19 11:29:53', '2020-04-19 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (113, '任攀', 'e10adc3949ba59abbe56e057f20f883e', '任攀', '121665841@qq.com', '2020-04-20 11:29:53', '2020-04-20 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (114, '王黎明', 'e10adc3949ba59abbe56e057f20f883e', '王黎明', '121665842@qq.com', '2020-04-21 11:29:53', '2020-04-21 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (115, '胡晓凤', 'e10adc3949ba59abbe56e057f20f883e', '胡晓凤', '121665843@qq.com', '2020-04-22 11:29:53', '2020-04-22 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (116, '曾文', 'e10adc3949ba59abbe56e057f20f883e', '曾文', '121665844@qq.com', '2020-04-23 11:29:53', '2020-04-23 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (117, '黄元江', 'e10adc3949ba59abbe56e057f20f883e', '黄元江', '121665845@qq.com', '2020-04-24 11:29:53', '2020-04-24 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (118, '葛仲琴', 'e10adc3949ba59abbe56e057f20f883e', '葛仲琴', '121665846@qq.com', '2020-04-25 11:29:53', '2020-04-25 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (119, '杨文', 'e10adc3949ba59abbe56e057f20f883e', '杨文', '121665847@qq.com', '2020-04-26 11:29:53', '2020-04-26 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (120, '黄梅', 'e10adc3949ba59abbe56e057f20f883e', '黄梅', '121665848@qq.com', '2020-04-27 11:29:53', '2020-04-27 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (121, '石春香', 'e10adc3949ba59abbe56e057f20f883e', '石春香', '121665849@qq.com', '2020-04-28 11:29:53', '2020-04-28 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (122, '宋学新', 'e10adc3949ba59abbe56e057f20f883e', '宋学新', '121665850@qq.com', '2020-04-29 11:29:53', '2020-04-29 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (123, '龙雪梅', 'e10adc3949ba59abbe56e057f20f883e', '龙雪梅', '121665851@qq.com', '2020-04-30 11:29:53', '2020-04-30 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (124, '唐燕峰', 'e10adc3949ba59abbe56e057f20f883e', '唐燕峰', '121665852@qq.com', '2020-05-01 11:29:53', '2020-05-01 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (125, '严子凌', 'e10adc3949ba59abbe56e057f20f883e', '严子凌', '121665853@qq.com', '2020-05-02 11:29:53', '2020-05-02 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (126, '冯文生', 'e10adc3949ba59abbe56e057f20f883e', '冯文生', '121665854@qq.com', '2020-05-03 11:29:53', '2020-05-03 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (127, '邬德政', 'e10adc3949ba59abbe56e057f20f883e', '邬德政', '121665855@qq.com', '2020-05-04 11:29:53', '2020-05-04 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (128, '刘余跃', 'e10adc3949ba59abbe56e057f20f883e', '刘余跃', '121665856@qq.com', '2020-05-05 11:29:53', '2020-05-05 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (129, '李成勇', 'e10adc3949ba59abbe56e057f20f883e', '李成勇', '121665857@qq.com', '2020-05-06 11:29:53', '2020-05-06 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (130, '汪伦', 'e10adc3949ba59abbe56e057f20f883e', '汪伦', '121665858@qq.com', '2020-05-07 11:29:53', '2020-05-07 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (131, '彭洪', 'e10adc3949ba59abbe56e057f20f883e', '彭洪', '121665859@qq.com', '2020-05-08 11:29:53', '2020-05-08 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (132, '唐文超', 'e10adc3949ba59abbe56e057f20f883e', '唐文超', '121665860@qq.com', '2020-05-09 11:29:53', '2020-05-09 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (133, '刘明礼', 'e10adc3949ba59abbe56e057f20f883e', '刘明礼', '121665861@qq.com', '2020-05-10 11:29:53', '2020-05-10 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (134, '王栓铭', 'e10adc3949ba59abbe56e057f20f883e', '王栓铭', '121665862@qq.com', '2020-05-11 11:29:53', '2020-05-11 11:29:55', 0);
INSERT INTO `tb_admin_user` VALUES (135, '李理', 'e10adc3949ba59abbe56e057f20f883e', '李理', '121665863@qq.com', '2020-05-12 11:29:53', '2020-05-12 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (136, '魏瑞', 'e10adc3949ba59abbe56e057f20f883e', '魏瑞', '121665864@qq.com', '2020-05-13 11:29:53', '2020-05-13 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (137, '唐勇', 'e10adc3949ba59abbe56e057f20f883e', '唐勇', '121665865@qq.com', '2020-05-14 11:29:53', '2020-05-14 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (138, '张志英', 'e10adc3949ba59abbe56e057f20f883e', '张志英', '121665866@qq.com', '2020-05-15 11:29:53', '2020-05-15 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (139, '游志明', 'e10adc3949ba59abbe56e057f20f883e', '游志明', '121665867@qq.com', '2020-05-16 11:29:53', '2020-05-16 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (140, '王峣', 'e10adc3949ba59abbe56e057f20f883e', '王峣', '121665868@qq.com', '2020-05-17 11:29:53', '2020-05-17 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (141, '吴晓题', 'e10adc3949ba59abbe56e057f20f883e', '吴晓题', '121665869@qq.com', '2020-05-18 11:29:53', '2020-05-18 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (142, '舒永熙', 'e10adc3949ba59abbe56e057f20f883e', '舒永熙', '121665870@qq.com', '2020-05-19 11:29:53', '2020-05-19 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (143, '刘小刚', 'e10adc3949ba59abbe56e057f20f883e', '刘小刚', '121665871@qq.com', '2020-05-20 11:29:53', '2020-05-20 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (144, '凌希云', 'e10adc3949ba59abbe56e057f20f883e', '凌希云', '121665872@qq.com', '2020-05-21 11:29:53', '2020-05-21 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (145, '姚建成', 'e10adc3949ba59abbe56e057f20f883e', '姚建成', '121665873@qq.com', '2020-05-22 11:29:53', '2020-05-22 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (146, '王龙平', 'e10adc3949ba59abbe56e057f20f883e', '王龙平', '121665874@qq.com', '2020-05-23 11:29:53', '2020-05-23 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (147, '许正权', 'e10adc3949ba59abbe56e057f20f883e', '许正权', '121665875@qq.com', '2020-05-24 11:29:53', '2020-05-24 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (148, '邹蔚', 'e10adc3949ba59abbe56e057f20f883e', '邹蔚', '121665876@qq.com', '2020-05-25 11:29:53', '2020-05-25 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (149, '李鑫', 'e10adc3949ba59abbe56e057f20f883e', '李鑫', '121665877@qq.com', '2020-05-26 11:29:53', '2020-05-26 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (150, '马永菡', 'e10adc3949ba59abbe56e057f20f883e', '马永菡', '121665878@qq.com', '2020-05-27 11:29:53', '2020-05-27 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (151, '周黎明', 'e10adc3949ba59abbe56e057f20f883e', '周黎明', '121665879@qq.com', '2020-05-28 11:29:53', '2020-05-28 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (152, '欧阳华杰', 'e10adc3949ba59abbe56e057f20f883e', '欧阳华杰', '121665880@qq.com', '2020-05-29 11:29:53', '2020-05-29 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (153, '赖欣婧', 'e10adc3949ba59abbe56e057f20f883e', '赖欣婧', '121665881@qq.com', '2020-05-30 11:29:53', '2020-05-30 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (154, '集团公司法律顾问', 'e10adc3949ba59abbe56e057f20f883e', '集团公司法律顾问', '121665882@qq.com', '2020-05-31 11:29:53', '2020-05-31 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (155, '阮昌益', 'e10adc3949ba59abbe56e057f20f883e', '阮昌益', '121665883@qq.com', '2020-06-01 11:29:53', '2020-06-01 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (156, '李文虎', 'e10adc3949ba59abbe56e057f20f883e', '李文虎', '121665884@qq.com', '2020-06-02 11:29:53', '2020-06-02 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (157, '江勇顺', 'e10adc3949ba59abbe56e057f20f883e', '江勇顺', '121665885@qq.com', '2020-06-03 11:29:53', '2020-06-03 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (158, '卢浩', 'e10adc3949ba59abbe56e057f20f883e', '卢浩', '121665886@qq.com', '2020-06-04 11:29:53', '2020-06-04 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (159, '李永林', 'e10adc3949ba59abbe56e057f20f883e', '李永林', '121665887@qq.com', '2020-06-05 11:29:53', '2020-06-05 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (160, '乔芮瑶', 'e10adc3949ba59abbe56e057f20f883e', '乔芮瑶', '121665888@qq.com', '2020-06-06 11:29:53', '2020-06-06 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (161, '尚宗飞', 'e10adc3949ba59abbe56e057f20f883e', '尚宗飞', '121665889@qq.com', '2020-06-07 11:29:53', '2020-06-07 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (162, '闫克俭', 'e10adc3949ba59abbe56e057f20f883e', '闫克俭', '121665890@qq.com', '2020-06-08 11:29:53', '2020-06-08 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (163, '任小康', 'e10adc3949ba59abbe56e057f20f883e', '任小康', '121665891@qq.com', '2020-06-09 11:29:53', '2020-06-09 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (164, '赵根', 'e10adc3949ba59abbe56e057f20f883e', '赵根', '121665892@qq.com', '2020-06-10 11:29:53', '2020-06-10 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (165, '刘涛', 'e10adc3949ba59abbe56e057f20f883e', '刘涛', '121665893@qq.com', '2020-06-11 11:29:53', '2020-06-11 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (166, '胡国玺', 'e10adc3949ba59abbe56e057f20f883e', '胡国玺', '121665894@qq.com', '2020-06-12 11:29:53', '2020-06-12 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (167, '曾崇勇', 'e10adc3949ba59abbe56e057f20f883e', '曾崇勇', '121665895@qq.com', '2020-06-13 11:29:53', '2020-06-13 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (168, '陈其学', 'e10adc3949ba59abbe56e057f20f883e', '陈其学', '121665896@qq.com', '2020-06-14 11:29:53', '2020-06-14 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (169, '杨露', 'e10adc3949ba59abbe56e057f20f883e', '杨露', '121665897@qq.com', '2020-06-15 11:29:53', '2020-06-15 11:29:55', 1);
INSERT INTO `tb_admin_user` VALUES (174, 'bourne-admin', 'e10adc3949ba59abbe56e057f20f883e', 'bourne-admin', '1216665820@qq.com', '2020-04-17 02:52:30', '2020-04-17 02:52:30', 0);

-- ----------------------------
-- Table structure for tb_blog_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_category`;
CREATE TABLE `tb_blog_category`  (
  `category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类表主键',
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类的名称',
  `category_icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类的图标',
  `category_rank` int(11) NOT NULL DEFAULT 1 COMMENT '分类的排序值 被使用的越多数值越大',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博文分类信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_category
-- ----------------------------
INSERT INTO `tb_blog_category` VALUES (1, 'About', '', 1, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_category` VALUES (20, 'JavaSE', '', 2, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_category` VALUES (22, 'JavaEE', '', 3, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_category` VALUES (24, 'MySQL', '', 4, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_category` VALUES (25, 'Springboot', '', 5, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_category` VALUES (26, 'SpringMVC', NULL, 6, 0, '2020-03-08 08:28:57');
INSERT INTO `tb_blog_category` VALUES (28, 'Spring', NULL, 7, 0, '2020-03-08 08:29:30');
INSERT INTO `tb_blog_category` VALUES (29, 'springCloud', '', 8, 0, '2020-03-08 08:29:53');
INSERT INTO `tb_blog_category` VALUES (30, '设计模式', '', 9, 0, '2020-04-18 13:50:26');
INSERT INTO `tb_blog_category` VALUES (31, 'Linux', NULL, 10, 0, '2020-04-18 13:50:44');
INSERT INTO `tb_blog_category` VALUES (32, 'docker', NULL, 11, 0, '2020-04-18 13:51:00');
INSERT INTO `tb_blog_category` VALUES (33, 'Nginx', '', 12, 0, '2020-04-18 13:59:56');
INSERT INTO `tb_blog_category` VALUES (34, '算法', NULL, 13, 0, '2020-04-18 14:00:10');
INSERT INTO `tb_blog_category` VALUES (35, 'websocket', NULL, 14, 0, '2020-04-18 14:21:06');
INSERT INTO `tb_blog_category` VALUES (36, 'zookper', NULL, 15, 0, '2020-04-18 14:21:36');
INSERT INTO `tb_blog_category` VALUES (37, 'rabbitMQ', NULL, 16, 0, '2020-04-18 14:22:22');
INSERT INTO `tb_blog_category` VALUES (38, 'Solr', '', 17, 0, '2020-04-18 14:22:46');
INSERT INTO `tb_blog_category` VALUES (40, '随笔', NULL, 20, 0, '2020-04-18 14:23:59');

-- ----------------------------
-- Table structure for tb_blog_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_comment`;
CREATE TABLE `tb_blog_comment`  (
  `comment_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `blog_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '关联的blog主键',
  `commentator` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论者名称',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论人的邮箱',
  `website_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '网址',
  `comment_body` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评论内容',
  `comment_create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论提交时间',
  `commentator_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '评论时的ip地址',
  `reply_body` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '回复内容',
  `reply_create_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  `comment_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否审核通过 0-未审核 1-审核通过',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0-未删除 1-已删除',
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博文评论信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_comment
-- ----------------------------
INSERT INTO `tb_blog_comment` VALUES (1, 7, '沐沐', '121665820@qq.com', '', '感觉还可以', '2020-03-01 10:01:50', '', '谢谢夸奖', '2020-03-01 09:53:00', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (2, 7, '紫宸', '121665820@qq.com', '', '不错', '2020-03-01 10:01:50', '', '感谢', '2020-03-01 09:53:00', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (3, 7, '柳木', '121665820@qq.com', '', '很好', '2020-03-01 10:01:50', '', '没问题', '2020-03-01 09:53:00', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (4, 7, '李白', '121665820@qq.com', '', '很厉害', '2020-03-01 10:01:50', '', '欢迎', '2020-04-28 09:25:42', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (5, 7, '张瑞艳', '121665820@qq.com', '', '哇瑟', '2020-03-01 10:01:50', '', 'okok', '2020-03-01 09:53:00', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (7, 7, '天兰', '121665820@qq.com', '', '还可以啊', '2020-03-01 10:01:50', '', '没关系', '2020-03-01 09:53:00', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (10, 1, '何兰', '121665820@qq.com', '', '厉害了', '2020-03-01 10:01:50', '', '你好啊，谢谢', '2020-03-01 09:53:00', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (12, 16, '陌陌', '12768463580@163.com', '', '学习了', '2020-04-18 16:52:13', '', '谢谢！', '2020-04-25 09:16:13', 1, 0);
INSERT INTO `tb_blog_comment` VALUES (13, 21, '喻杰', '15152245@qq.com', '', ' nginx怎么配置的', '2020-05-10 15:58:17', '', '', NULL, 1, 0);

-- ----------------------------
-- Table structure for tb_blog_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_config`;
CREATE TABLE `tb_blog_config`  (
  `config_field` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段名',
  `config_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置名',
  `config_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置项的值',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`config_field`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客系统配置信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_config
-- ----------------------------
INSERT INTO `tb_blog_config` VALUES ('icp', 'ICP备案/许可证编号', '蜀ICP备 19040520号', '2020-04-27 14:09:49', '2020-04-28 09:08:19');
INSERT INTO `tb_blog_config` VALUES ('qcr', '系统二维码', 'http://122.51.85.120:8080/blog/myblog/QCR.png', '2020-04-17 06:36:20', '2020-05-11 09:24:32');
INSERT INTO `tb_blog_config` VALUES ('sysAuthor', '开发者', '阳沐之', '2020-03-01 10:01:50', '2020-03-01 10:01:50');
INSERT INTO `tb_blog_config` VALUES ('sysAuthorImg', '开发者头像', 'http://122.51.85.120:8080/blog/authorImg/20200418_13225137.jpg', '2020-03-01 10:01:50', '2020-04-18 13:22:51');
INSERT INTO `tb_blog_config` VALUES ('sysCopyRight', '版权所有', '阳沐之', '2020-03-01 10:01:50', '2020-03-01 11:39:02');
INSERT INTO `tb_blog_config` VALUES ('sysEmail', '开发者邮箱', '17882555101@163.com', '2020-03-01 10:01:50', '2020-03-01 10:01:50');
INSERT INTO `tb_blog_config` VALUES ('sysUpdateTime', '最后修改时间', '2020年4月1日17:59:18', '2020-03-01 10:01:50', '2020-04-19 05:49:17');
INSERT INTO `tb_blog_config` VALUES ('sysUrl', '服务器url', 'http://122.51.85.120:8080/blog', '2020-03-01 10:01:50', '2020-04-28 10:00:08');
INSERT INTO `tb_blog_config` VALUES ('sysVersion', '当前版本号', '0.0.1', '2020-03-01 10:01:50', '2020-03-01 10:01:50');
INSERT INTO `tb_blog_config` VALUES ('sysWord', '至理名言', '一晃两三年，匆匆又夏天.', '2020-03-05 06:16:50', '2020-03-05 06:26:46');
INSERT INTO `tb_blog_config` VALUES ('websiteName', '博客名', '阳沐之个人博客', '2020-03-01 10:01:50', '2020-04-28 12:24:57');

-- ----------------------------
-- Table structure for tb_blog_file
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_file`;
CREATE TABLE `tb_blog_file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件url',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `type` tinyint(2) NULL DEFAULT NULL COMMENT '文件类型，01：视频，02：图片，03：普通，04 系统二维码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客系统文件信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_file
-- ----------------------------
INSERT INTO `tb_blog_file` VALUES (5, '系统二维码', 'http://122.51.85.120:8080/blog/myblog/QCR.png', '2020-03-08 15:41:57', '2020-05-11 09:24:32', 4);
INSERT INTO `tb_blog_file` VALUES (45, '头像_20200418_13225137.jpg', 'http://122.51.85.120:8080/blog/authorImg/20200418_13225137.jpg', '2020-04-18 13:22:51', '2020-04-18 13:22:51', 2);
INSERT INTO `tb_blog_file` VALUES (48, '1000道 互联网大厂Java工程师面试题.pdf', 'http://122.51.85.120:8080/blog/myfile/20200420_130053_1000道 互联网大厂Java工程师面试题.pdf', '2020-04-20 13:00:54', '2020-04-20 13:00:54', 3);
INSERT INTO `tb_blog_file` VALUES (49, 'JVM指令手册 (1).pdf', 'http://122.51.85.120:8080/blog/myfile/20200421_005805_JVM指令手册 (1).pdf', '2020-04-21 00:58:06', '2020-04-21 00:58:06', 3);
INSERT INTO `tb_blog_file` VALUES (50, 'CTF入门手册.pdf', 'http://122.51.85.120:8080/blog/myfile/20200421_010002_CTF入门手册.pdf', '2020-04-21 01:00:03', '2020-04-21 01:00:03', 3);
INSERT INTO `tb_blog_file` VALUES (51, '无线网络修复工具V3.14.1.exe', 'http://122.51.85.120:8080/blog/myfile/20200421_010317_无线网络修复工具V3.14.1.exe', '2020-04-21 01:03:18', '2020-04-21 01:03:18', 3);
INSERT INTO `tb_blog_file` VALUES (52, 'SoftMgr_Setup_31_1442.exe', 'http://122.51.85.120:8080/blog/myfile/20200421_050627_SoftMgr_Setup_31_1442.exe', '2020-04-21 05:06:27', '2020-04-21 05:06:27', 3);
INSERT INTO `tb_blog_file` VALUES (53, '西南民大-会议系统需求确认单（4.17）-新.docx', 'http://www.yibin.pub:8080/blog/myfile/20200421_062219_西南民大-会议系统需求确认单（4.17）-新.docx', '2020-04-21 06:22:19', '2020-04-21 06:22:19', 3);
INSERT INTO `tb_blog_file` VALUES (56, 'lbuilder__93859_1588080637.apk', 'http://122.51.85.120:8080/blog/myfile/20200428_133132_lbuilder__93859_1588080637.apk', '2020-04-28 13:31:32', '2020-04-28 13:31:32', 3);
INSERT INTO `tb_blog_file` VALUES (58, 'vidsplay-campfire-embers.mp4', 'http://122.51.85.120:8080/blog/myfile/20200508_124009_vidsplay-campfire-embers.mp4', '2020-05-08 12:40:10', '2020-05-08 12:40:10', 1);
INSERT INTO `tb_blog_file` VALUES (60, 'abstract-spinning-orb-1.mp4', 'http://122.51.85.120:8080/blog/myfile/20200508_125233_abstract-spinning-orb-1.mp4', '2020-05-08 12:52:33', '2020-05-08 12:52:33', 1);

-- ----------------------------
-- Table structure for tb_blog_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_info`;
CREATE TABLE `tb_blog_info`  (
  `blog_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '博客表主键id',
  `blog_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客标题',
  `blog_sub_url` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客自定义路径url',
  `blog_preface` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客前言',
  `blog_content` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客内容',
  `blog_category_id` int(11) NULL DEFAULT NULL COMMENT '博客分类id',
  `blog_category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客分类',
  `blog_tags` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客标签',
  `blog_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-草稿 1-发布',
  `blog_views` bigint(20) NOT NULL DEFAULT 0 COMMENT '阅读量',
  `email_comment` tinyint(1) NULL DEFAULT NULL COMMENT '是否 邮件推送评论0=否 1=是',
  `enable_comment` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-允许评论 1-不允许评论',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`blog_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博文信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_info
-- ----------------------------
INSERT INTO `tb_blog_info` VALUES (1, '我是阳沐之', 'about', '关于自己的简短介绍', '## About me\n\n我是阳沐之:tw-1f471:，一名很普通的大四学生，这个博客写文章的初衷是为了记录自己的成长和锻炼自己。\n\n成长的路总是跌跌撞撞的，从大一到现在，也快毕业了。在大学期间，迷茫、彷徨过；有过质疑自己的时候，甚至一度想就这样放弃……这个世界从不曾温情脉脉，也没有什么岁月静好，你我必须要非常努力才能争取到自己的一席之地。\n\n梦想永在，你不努力，谁也给不了你想要的生活。反问自己没有梦想，何必远方？\n\n相信浏览这段话的你也知道，学习是一件极其枯燥而无聊的过程，甚至有时候显得很无助，我也想告诉你，成长就是这样一件残酷的事情，任何成功都不是一蹴而就，需要坚持、需要付出、需要你的毅力，短期可能看不到收获，因为破茧成蝶所耗费的时间不是一天。\n\n最后，2020无畏年少青春，迎风潇洒前行！！\n## Contact\n\n我的邮箱：17882555101@163.com\nQQ：121665820\n欢迎加我交流', 1, 'About', '生活,学习', 1, 526, 1, 0, 0, '2020-04-01 11:58:43', '2020-04-28 11:58:43');
INSERT INTO `tb_blog_info` VALUES (7, 'redis整合', 'redis', 'redis高性能数据库', '前面我已经写过一篇博客简单介绍Redis的Java客户端和Redis的一整合操作，这里我们深入将Redis配置SpringCache注解来实现缓存，这样效率更高更快捷\n\n话不多说，直接晒代码\n\n一、redis配置文件\n```java\nredis.host=192.168.3.143\nredis.port=6379\nredis.dbIndex=1\nredis.expiration=3000\nredis.maxIdle=300\nredis.maxTotal=600\nredis.maxWaitMillis=1000\nredis.testOnBorrow=true\nredis.password=有密码就配，没有就不用\n```\n\n二、Spring-redis.xml文件\n\n\n```xml\n    <?xml version=\"1.0\" encoding=\"UTF-8\"?>\n    <beans xmlns=\"http://www.springframework.org/schema/beans\"\n           xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n           xmlns:context=\"http://www.springframework.org/schema/context\"\n           xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd\">\n    \n        <context:property-placeholder location=\"classpath:redis.properties\" ignore-unresolvable=\"true\" order=\"2\"></context:property-placeholder>\n        <!-- 扫描RedisCacheConfig  -->\n        <context:component-scan base-package=\"com.zl.config\" ></context:component-scan>\n    \n        <bean id=\"jedisPoolConfig\" class=\"redis.clients.jedis.JedisPoolConfig\">\n            <property name=\"maxIdle\" value=\"${redis.maxIdle}\"></property>\n            <property name=\"maxTotal\" value=\"${redis.maxTotal}\"></property>\n            <property name=\"maxWaitMillis\" value=\"${redis.maxWaitMillis}\"></property>\n            <property name=\"testOnBorrow\" value=\"${redis.testOnBorrow}\"></property>\n        </bean>\n    \n        <bean id=\"jedisConnectionFactory\" class=\"org.springframework.data.redis.connection.jedis.JedisConnectionFactory\">\n            <property name=\"hostName\" value=\"${redis.host}\"></property>\n            <property name=\"port\" value=\"${redis.port}\"></property>\n            <property name=\"database\" value=\"${redis.dbIndex}\"></property>\n            <property name=\"password\" value=\"${redis.password}\"></property>\n            <property name=\"poolConfig\" ref=\"jedisPoolConfig\"></property>\n        </bean>\n    \n        <bean id=\"redisTemplate\" class=\"org.springframework.data.redis.core.RedisTemplate\">\n            <property name=\"connectionFactory\" ref=\"jedisConnectionFactory\"></property>\n            <!--   key进行序列化配置，默认JDK改为String     -->\n            <property name=\"keySerializer\">\n                <bean class=\"org.springframework.data.redis.serializer.StringRedisSerializer\"></bean>\n            </property>\n            <property name=\"valueSerializer\">\n                <bean class=\"org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer\"></bean>\n            </property>\n            <property name=\"hashKeySerializer\">\n                <bean class=\"org.springframework.data.redis.serializer.StringRedisSerializer\"></bean>\n            </property>\n        </bean>\n    \n        <!-- 配置RedisCacheManager -->\n        <bean id=\"redisCacheManager\" class=\"org.springframework.data.redis.cache.RedisCacheManager\">\n            <constructor-arg name=\"redisOperations\" ref=\"redisTemplate\"></constructor-arg>\n            <!-- 配置过期时间-->\n            <property name=\"defaultExpiration\" value=\"3000\"></property>\n        </bean>\n    \n        <!-- 配置RedisCacheConfig -->\n        <bean id=\"redisCacheConfig\" class=\"com.zl.config.RedisCacheConfig\">\n            <constructor-arg ref=\"jedisConnectionFactory\"></constructor-arg>\n            <constructor-arg ref=\"redisTemplate\"></constructor-arg>\n            <constructor-arg ref=\"redisCacheManager\"></constructor-arg>\n        </bean>\n    </beans>\n```\n\n这里要注意valueSerializer的序列化方式，建议使用GenericJackson2JsonRedisSerializer，因为使用StringRedisSerializer在存入对象时会报类型转换异常，且使用GenericJackson2JsonRedisSerializer在Redis可视化工具RedisDesktopManager也可以看到不是乱码而是Json格式。效果如下：\n\n\n\n三、RedisCacheConfig配置类\n```java\npackage com.zl.config;\n\nimport org.springframework.cache.annotation.CachingConfigurerSupport;\nimport org.springframework.cache.annotation.EnableCaching;\nimport org.springframework.cache.interceptor.KeyGenerator;\nimport org.springframework.context.annotation.Bean;\nimport org.springframework.context.annotation.Configuration;\nimport org.springframework.data.redis.cache.RedisCacheManager;\nimport org.springframework.data.redis.connection.jedis.JedisConnectionFactory;\nimport org.springframework.data.redis.core.RedisTemplate;\n\nimport java.lang.reflect.Method;\n\n/**\n * @program: FruitSales\n * @classname: RedisCacheConfig\n * @description: Redis缓存配置\n * @create: 2019-07-01 18:56\n **/\n@Configuration\n@EnableCaching\npublic class RedisCacheConfig extends CachingConfigurerSupport {\n\n    private volatile JedisConnectionFactory jedisConnectionFactory;\n    private volatile RedisTemplate<String, String> redisTemplate;\n    private volatile RedisCacheManager redisCacheManager;\n\n    public RedisCacheConfig() {\n        super();\n    }\n\n    /**\n     * 带参数的构造方法 初始化所有的成员变量\n     *\n     * @param jedisConnectionFactory\n     * @param redisTemplate\n     * @param redisCacheManager\n     */\n    public RedisCacheConfig(JedisConnectionFactory jedisConnectionFactory, RedisTemplate<String, String> redisTemplate,\n                            RedisCacheManager redisCacheManager) {\n        this.jedisConnectionFactory = jedisConnectionFactory;\n        this.redisTemplate = redisTemplate;\n        this.redisCacheManager = redisCacheManager;\n    }\n\n    public JedisConnectionFactory getJedisConnecionFactory() {\n        return jedisConnectionFactory;\n    }\n\n    public RedisTemplate<String, String> getRedisTemplate() {\n        return redisTemplate;\n    }\n\n    public RedisCacheManager getRedisCacheManager() {\n        return redisCacheManager;\n    }\n\n    @Bean\n    public KeyGenerator keyGenerator() {\n        return new KeyGenerator() {\n            @Override\n            public Object generate(Object target, Method method,\n                                   Object... params) {\n                //规定  本类名+方法名+参数名 为key\n                StringBuilder sb = new StringBuilder();\n                sb.append(target.getClass().getName()+\"_\");\n                sb.append(method.getName()+\"_\");\n                for (Object obj : params) {\n                    sb.append(obj.toString()+\",\");\n                }\n                return sb.toString();\n            }\n        };\n    }\n}\n\n```\n四、接下来只要在需要缓存的方式上加注释啦\n如这样:在service方法上加@Cacheable注解\n\n\n\n常用注解：\n\n@Cacheable、@CachePut、@CacheEvict 注释介绍\n通过上面的例子，我们可以看到 spring cache 主要使用两个注释标签，即 @Cacheable、@CachePut 和 @CacheEvict，我们总结一下其作用和配置方法。', 22, 'JavaEE', 'redis', 1, 85, 1, 0, 0, '2020-05-11 07:51:17', '2020-05-11 07:51:15');
INSERT INTO `tb_blog_info` VALUES (8, 'vi常用命令', 'tesy', 'vi常用命令', '**vi/vim 的使用**\n vi/vim 三种模式，分别是命令模式（Command mode），输入模式（Insert mode）和底线命令模式（Last line mode）。 这三种模式的作用分别是：\n\n**命令模式：**\nESC便进入了命令模式。\n\n此状态下敲击键盘动作会被Vim识别为命令，而非输入字符。比如我们此时按下aoi，并不会输入一个字符，i被当作了一个命令。\n\n以下是常用的几个命令：\n\ni 切换到输入模式，以输入字符。\nx 删除当前光标所在处的字符。\n: 切换到底线命令模式，以在最底一行输入命令。\n若想要编辑文本：启动Vim，进入了命令模式，按下i，切换到输入模式。\n\n命令模式只有一些最基本的命令，因此仍要依靠底线命令模式输入更多命令。\n\n**输入模式**\n在命令模式下按下i就进入了输入模式。\n\n在输入模式中，可以使用以下按键：\n\n字符按键以及Shift组合，输入字符\n\n```java\nENTER，回车键，换行\nBACK SPACE，退格键，删除光标前一个字符\nDEL，删除键，删除光标后一个字符\n方向键，在文本中移动光标\nHOME/END，移动光标到行首/行尾\nPage Up/Page Down，上/下翻页\nInsert，切换光标为输入/替换模式，光标将变成竖线/下划线\nESC，退出输入模式，切换到命令模式\n```\n\n**底线命令模式**\n在命令模式下按下:进入了底线命令模式。\n在底线命令模式中，基本的命令有：\n\nq 退出程序\nw 保存文件\n按ESC键可随时退出底线命令模式。\n\n  \n  \n**命令模式键盘图**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20200413220923106.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n\n\n**权限操作**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/2020041322192250.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n\n```java\n-a 所有用户\n```\n', 31, 'Linux', 'Linux', 1, 35, 1, 0, 0, '2020-04-19 05:54:11', '2020-04-19 05:54:11');
INSERT INTO `tb_blog_info` VALUES (11, '查看CentOS系统配置情况命令', 'test', '查看CentOS系统配置情况命令', '```java\n系统\n# uname -a               # 查看内核/操作系统/CPU信息\n# head -n 1 /etc/issue   # 查看操作系统版本\n# cat /proc/cpuinfo      # 查看CPU信息\n# hostname               # 查看计算机名\n# lspci -tv              # 列出所有PCI设备\n# lsusb -tv              # 列出所有USB设备\n# lsmod                  # 列出加载的内核模块\n# env                    # 查看环境变量\n#dmidecode | grep \"Product Nmae\"   #查看服务器型号\n资源\n# free -m                # 查看内存使用量和交换区使用量\n# df -h                  # 查看各分区使用情况\n# du -sh <目录名>        # 查看指定目录的大小\n# grep MemTotal /proc/meminfo   # 查看内存总量\n# grep MemFree /proc/meminfo    # 查看空闲内存量\n# uptime                 # 查看系统运行时间、用户数、负载\n# cat /proc/loadavg      # 查看系统负载\n磁盘和分区\n\n# mount | column -t      # 查看挂接的分区状态\n# fdisk -l               # 查看所有分区\n# swapon -s              # 查看所有交换分区\n# hdparm -i /dev/hda     # 查看磁盘参数(仅适用于IDE设备)\n# dmesg | grep IDE       # 查看启动时IDE设备检测状况\n网络\n\n# ifconfig               # 查看所有网络接口的属性\n# iptables -L            # 查看防火墙设置\n# route -n               # 查看路由表\n# netstat -lntp          # 查看所有监听端口\n# netstat -antp          # 查看所有已经建立的连接\n# netstat -s             # 查看网络统计信息\n进程\n\n# ps -ef                 # 查看所有进程\n# top                    # 实时显示进程状态\n用户\n\n# w                      # 查看活动用户\n# id <用户名>            # 查看指定用户信息\n# last                   # 查看用户登录日志\n# cut -d: -f1 /etc/passwd   # 查看系统所有用户\n# cut -d: -f1 /etc/group    # 查看系统所有组\n# crontab -l             # 查看当前用户的计划任务\n服务\n\n# chkconfig --list       # 列出所有系统服务\n# chkconfig --list | grep on    # 列出所有启动的系统服务\n程序\n\n# rpm -qa                # 查看所有安装的软件包\n```\n', 31, 'Linux', 'Linux', 1, 31, 1, 0, 0, '2020-04-01 05:54:50', '2020-04-19 05:54:50');
INSERT INTO `tb_blog_info` VALUES (12, 'nginx搭建文件服务器', '', 'nginx搭建文件服务器', '#### 一：为什么使用Nginx？\n 在传统的Web项目中，并发量小，用户使用的少。所以在低并发的情况下，用户可以直接访问tomcat服务器，然后tomcat服务器返回消息给用户。比如，我们上传图片：\n ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200328192519968.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n当然我们知道，为了解决并发，可以使用负载均衡：也就是我们多增加几个tomcat服务器。当用户访问的时候，请求可以提交到空闲的tomcat服务器上。\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20200328192609996.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n 但是这种情况下可能会有一种这样的问题：上传图片操作。我们把图片上传到了tomcat1上了，当我们要访问这个图片的时候，tomcat1正好在工作，所以访问的请求就交给其他的tomcat操作，而tomcat之间的数据没有进行同步，所以就发生了我们要请求的图片找不到。\n为了解决这种情况，我们就想出了分布式。我们专门建立一个图片服务器，用来存储图片。这样当我们都把图片上传的时候，不管是哪个服务器接收到图片，都把图片上传到图片服务器。\n图片服务器上需要安装一个http服务器，可以使用tomcat、apache、nginx，项目需要可以使用ftp上传文件。\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20200328192737762.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n看到这里大家可能会问，既然我们要选择的是http服务器，为什么不继续使用tomcat，而要使用Nginx？\n**原因如下：** nginx常用做静态内容服务和代理服务器（不是你FQ那个代理），直面外来请求转发给后面的应用服务（tomcat，django什么的），tomcat更多用来做做一个应用容器，让java web app跑在里面的东西，对应同级别的有jboss,jetty等东西。\n### 搭建服务器\n- 搭建ftp服务器略\n\n##### Nginx的配置\n\n![在/usr/nginx/conf目录下nginx.conf文件是nginx的配置文件。](https://img-blog.csdnimg.cn/20200328193057797.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n#### 端口和目录的配置\n\n 在nginx.conf文件中添加一个server节点，修改端口号就可以【自行添加。不影响原来的】也就是一个nginx里面可以跑多个端口的项目，这个是tomcat是有本质的区别的\n一个tomcat只能对应一个端口的多个项目服务\n一个nginx可以对应多个端口下面的多个项目服务\n\n```java\n #ftp\n    server {\n        listen       9999;\n        server_name  ip;\n        location / {\n            root   /data/wwwroot/default;\n        }\n        error_page   500 502 503 504  /50x.html;\n        location = /50x.html {\n            root   html;\n        }\n   }\n```\n**效果**  \n![在这里插入图片描述](https://img-blog.csdnimg.cn/2020032819343441.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)', 33, 'Nginx', 'Linux', 1, 22, 1, 0, 0, '2020-04-19 05:53:47', '2020-04-19 05:53:47');
INSERT INTO `tb_blog_info` VALUES (13, 'tomcat多域名多端口访问', '', 'tomcat多域名多端口访问', '### tomcat默认localhost访问\n#### 1.多域名访问：\n```java\n原因：host文件配置了默认主机映射->127.0.0.1  localhost\n```\n我做了如下映射：\n\n```java\n127.0.0.1  me.local\n```\n访问：\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20200317214858855.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n#### 2，多端口访问\n添加service\n\n```java\n  </Service>\n    <Service name=\"Catalina\">\n     <Connector connectionTimeout=\"20000\" port=\"80\" protocol=\"HTTP/1.1\" redirectPort=\"8443\"/>\n    <Connector port=\"8009\" protocol=\"AJP/1.3\" redirectPort=\"8443\"/>\n\n    <Engine defaultHost=\"localhost\" name=\"Catalina\">\n      <Realm className=\"org.apache.catalina.realm.LockOutRealm\">\n        <Realm className=\"org.apache.catalina.realm.UserDatabaseRealm\" resourceName=\"UserDatabase\"/>\n      </Realm>\n\n      <Host appBase=\"webapps\" autoDeploy=\"true\" name=\"me.local\" unpackWARs=\"true\">\n\n        <Valve className=\"org.apache.catalina.valves.AccessLogValve\" directory=\"logs\" pattern=\"%h %l %u %t \"%r\" %s %b\" prefix=\"localhost_access_log\" suffix=\".txt\"/>\n  </Host>\n    </Engine>\n  </Service>\n```\n端口设置为80，主机名设置为me.local\n**启动**tomcat时，我们可以短刀tomcat启动了80端口和8080端口。\n访问：\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20200317215121744.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)', 22, 'JavaEE', 'tomcat', 1, 17, 1, 0, 0, '2020-04-19 05:53:28', '2020-04-19 05:53:28');
INSERT INTO `tb_blog_info` VALUES (14, '非root用户用80端口启动程序', '', '非root用户用80端口启动程序', '默认情况下Linux的1024以下端口是只有root用户才有权限占用，我们的tomcat，apache，\nnginx等等程序如果想要用普通用户来占用80端口的话就会抛出 java.net.BindException: Permission denied的异常。 \n**解决办法有三种：** \n\n1.  使用非80端口启动程序，然后再用iptables做一个端口转发。 \n    iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080 \n    用root用户直接去执行就可以了!\n  \n2. 使用nginx转发，当然nginx需要使用root权限\n\n\n3. 使用root权限直接运行tomcat\n\n```\n第一种方法步骤：\n方式1.配置tomcat80端口后，使用root权限用户执行下面命令：\n开放8081访问权限：\nsudo iptables -I INPUT -p tcp --dport 8081 -j ACCEPT\n开启端口转发\nsudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8081\n```\n添加了端口转发后，如果重启了防火墙，端口转发会被重置，可以使用如下命令将该规则保存在iptables里\nservice iptables save\n\n```\n方式2.防火墙添加(需要使用root用户)\nvim /etc/sysconfig/iptables\n添加如下命令\n-A INPUT -p tcp -m tcp --dport 8080 -m state --state NEW -j ACCEPT\n保存后重启防火墙\nservice iptables restart\n\n```\n\n```\n注：如果不能拿到root用户，而有sudo权限，则对防火墙相关的操作命令前面都加上sudo即可，例如：\n\nsudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8081\n\nsudo vim /etc/sysconfig/iptables\nsudo service iptables restart\n\n```\n', 22, 'JavaEE', 'tomcat', 1, 43, 1, 0, 0, '2020-04-19 05:55:04', '2020-04-19 05:55:04');
INSERT INTO `tb_blog_info` VALUES (15, 'EasyCode代码生成插件用法', '', 'EasyCode代码生成插件用法', '**Esay  Code：**\n基于IntelliJ IDEA开发的代码生成插件，支持自定义任意模板（Java，html，js，xml）。\n只要是与数据库相关的代码都可以通过自定义模板来生成。支持数据库类型与java类型映射关系配置。\n支持同时生成生成多张表的代码。每张表有独立的配置信息。完全的个性化定义，规则由你设置。\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20191130152027832.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n详情见说明文档：[https://gitee.com/makejava/EasyCode/wikis/pages?sort_id=725164&doc_id=166248](https://gitee.com/makejava/EasyCode/wikis/pages?sort_id=725164&doc_id=166248)', 25, 'Springboot', '笔记', 1, 74, 1, 0, 0, '2020-04-06 05:55:49', '2020-04-19 05:55:49');
INSERT INTO `tb_blog_info` VALUES (16, 'Springboot定时任务', '', 'Springboot定时任务', '1，添加依赖\n\n```\n        <!-- 定时任务-->\n        <dependency>\n            <groupId>org.springframework.boot</groupId>\n            <artifactId>spring-boot-starter-actuator</artifactId>\n            <version>1.5.13.RELEASE</version>\n        </dependency>\n```\n2，添加注解到Springbootapplication上\n\n```\n@EnableScheduling\n```\n3，编写定时任务\n@Component\npublic class TaskCheck {\n\n    private static final Logger log = LoggerFactory.getLogger(TaskCheck.class);\n    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(\"HH:mm:ss\");\n\n    @Scheduled(cron = \"0 0 0 * * ?\")\n    //cron表达式，每晚12点执行，按需更改\n    public void reportCurrentTime() {\n        log.info(\"The time is now {}\", dateFormat.format(new Date()));\n        //代码块\n    }\n\n}', 25, 'Springboot', '高阶', 1, 69, 1, 0, 0, '2020-04-15 05:56:20', '2020-04-19 05:56:20');
INSERT INTO `tb_blog_info` VALUES (17, '单例设计模式', '', '单例设计模式', '单例设计模式：\n\n意图：保证一个类仅有一个实例，并提供一个访问它的全局访问点。\n\n主要解决：一个全局使用的类频繁地创建与销毁。\n\n何时使用：当您想控制实例数目，节省系统资源的时候。\n\n如何解决：判断系统是否已经有这个单例，如果有则返回，如果没有则创建。\n\n关键代码：构造函数是私有的。\n\n\n\n单例模式的从实现步骤上来讲，分为三步：\n\n构造方法私有，保证无法从外部通过 new 的方式创建对象。\n对外提供获取该类实例的静态方法\n类的内部创建该类的对象，通过第 2 步的静态方法返回\n\n\n单例模式包括饿汉和懒汉，懒汉不安全。\n\n懒汉方式。指全局的单例实例在第一次被使用时构建。\n\n饿汉方式。指全局的单例实例在类装载时构建。\n\n\n\n饿汉单例设计模式：\n        1. 私有化构造函数。\n        2. 声明本类的引用类型变量并且指向本类的对象，(private static)\n        3. 提供一个公共静态的方法获取本类对象。 \n    实现：\n\npublic class BasicSingleTon {\n\n    //创建唯一实例\n    private static final BasicSingleTon instance = new BasicSingleTon();\n\n    //第二部暴露静态方法返回唯一实例\n    public static BasicSingleTon getInstance() {\n        return instance;\n    }\n\n    //第一步构造方法私有\n    private BasicSingleTon() {\n    }\n}\n\n懒汉单例设计模式:\n        1. 私有化构造函数。\n        2.  声明本类的引用类型变量，但是不要创建对象。\n        3. 提供一个公共静态的方法获取本类对象，获取之前先判断是否已经创建了本类的对象，\n        如果没有创建，创建再返回。如果已经创建了，那么直接访问即可。\n\n实现：\n\npublic class LazyBasicSingleTon {\n\n    private static LazyBasicSingleTon singleTon = null;\n\n    public static LazyBasicSingleTon getInstance() {\n        //延迟初始化 在第一次调用 getInstance 的时候创建对象\n        if (singleTon == null) {\n            singleTon = new LazyBasicSingleTon();\n        }\n\n        return singleTon;\n    }\n\n    private LazyBasicSingleTon() {\n    }\n}\n', 30, '设计模式', '笔记', 1, 108, 1, 0, 0, '2020-04-19 05:56:41', '2020-04-19 05:56:41');
INSERT INTO `tb_blog_info` VALUES (18, 'Lambda表达式总结（Java8新特性）', '', 'Lambda表达式总结（Java8新特性）', '从java8开始没有接口的实现类，也可以直接使用接口：\nambda表达式，lambda表达式替代了实现类\n\n**lambda使用前提：**\n一定要有函数式接口才能使用，没有函数式接口不仅能使用lambda表达式\n\n**函数接口：**\n有且仅有一个抽象方法的接口，无所谓有没有@functionalIterface,这是一个可选的检测手段而已\n\n**使用lambda的推断环境：**\na)要么根据参数类型传参来推断函数式接口\nb）要么根据赋值语句左侧类型来推断函数接口\n\n**lambda表达式的简便格式：**\n（int num）——>{return ++num}\n（num）->{return ++num}\n num->{return ++num}\n num->++num\n\n a)参数类型可以省略\n b）如果有且仅有一个参数，那么小括号可以省略\n c)如果有且仅有一个语句，那么大括号和return也可以省略\n \n注：Lambda表达式常于方法引用（：：）结合使用。', 22, 'JavaEE', 'Java', 1, 26, 1, 0, 0, '2020-04-05 05:57:15', '2020-04-19 07:50:50');
INSERT INTO `tb_blog_info` VALUES (19, '堆参数调优', '', '堆参数调优', '**1.Heap堆（Java7之前）：**\n\n一个JVM实例只存在一个堆内存，堆内存的大小是可以调节的。类加载器读取了类文件后，需要把类、方法、常变量放到堆内存中，保存所有引用类型的真实信息，以方便执行器执行。\n\n **堆内存逻辑上分为三部分：新生+养老+永久**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524093419214.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n**新生区**\n	新生区是类的诞生、成长、消亡的区域，一个类在这里产生，应用，最后被垃圾回收器收集，结束生命。\n	**新生区又分为两部分**： 伊甸区（Eden space）和幸存者区（Survivor pace） ，所有的类都是在伊甸区被new出来的。幸存区有两个： 0区（Survivor 0 space）和1区（Survivor 1 space）。\n\n当伊甸园的空间用完时，程序又需要创建对象，JVM的垃圾回收器将对伊甸园区进行垃圾回收(Minor GC)，将伊甸园区中的不再被其他对象所引用的对象进行销毁。然后将伊甸园中的剩余对象移动到幸存0区.若幸存0区也满了，再对该区进行垃圾回收，然后移动到1区。那如果1区也满了呢？再移动到养老区。若养老区也满了，那么这个时候将产生MajorGC（FullGC），进行养老区的内存清理。若养老区执行了Full GC之后发现依然无法进行对象的保存，就会产生OOM异常“OutOfMemoryError”。(注意：永久存储区是逻辑划分的，实际上不存在）\n\n如果出现java.lang.OutOfMemoryError: Java heap space异常，说明Java虚拟机的堆内存不够。**原因**有二：\n（1）Java虚拟机的堆内存设置不够，可以通过参数-Xms、-Xmx来调整。\n（2）代码中创建了大量大对象，并且长时间不能被垃圾收集器收集（存在被引用）。\n\n**2.Heap堆（1.8以后）：**\n永久存储区永久取消，更名为元空间Meterbase。\n**Java7**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/2019052409415169.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n**Java8：**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524094223970.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n**4，堆内存调优简介：**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524094332434.png)\n**测试代码：**\n```\npublic static void main(String[] args){\nlong maxMemory = Runtime.getRuntime().maxMemory() ;//返回 Java 虚拟机试图使用的最大内存量。\nlong totalMemory = Runtime.getRuntime().totalMemory() ;//返回 Java 虚拟机中的内存总量。\nSystem.out.println(\"MAX_MEMORY = \" + maxMemory + \"（字节）、\" + (maxMemory / (double)1024 / 1024) + \"MB\");\nSystem.out.println(\"TOTAL_MEMORY = \" + totalMemory + \"（字节）、\" + (totalMemory / (double)1024 / 1024) + \"MB\");\n}\n\n```\n通过测试，我们发现：\n发现默认的情况下分配的内存是总内存的“1 / 4”、而初始化的内存为“1 / 64”\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524094556615.png)\nVM参数：	-Xms1024m -Xmx1024m -XX:+PrintGCDetails\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524094640696.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n运行结果：\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524094653213.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n**如何生成dump文件，快速定位内存泄漏：**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/2019052409471051.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n**步骤：**\n1。复制：\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524094813155.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n2。install\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524094904127.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n3。产生HeapDumpOnOutOfMemoryError\nOOM时导出堆到hprof文件。\n编写产生OOM代码：\n```\npublic class OOM {\n	byte []s=new byte[1*1024*1024];\n		public static void main(String[] args) {\n			ArrayList<OOM> list=new ArrayList<OOM>();\n			\n			byte []s=new byte[1*1024*1024];\n			for (int i = 0; i < 100; i++) {\n				\n				list.add(new OOM());\n			}\n		}\n}\n```\n\nVM参数：	\n-Xms1m -Xmx8m -XX:+HeapDumpOnOutOfMemoryError\n\n**4。结果**\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524104824693.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n![在这里插入图片描述](https://img-blog.csdnimg.cn/20190524095059135.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1Mzg1Njg3,size_16,color_FFFFFF,t_70)\n', 22, 'JavaEE', 'jvm', 1, 27, 1, 0, 0, '2020-04-19 05:55:25', '2020-04-19 05:55:25');
INSERT INTO `tb_blog_info` VALUES (20, 'Jquery获取和修改img的src值的方法', '', 'Jquery获取和修改img的src值的方法', '获取：\n   ```javascript\n$(\"#imgId\")[0].src;\n```\n修改：\n```javaScript\n$(\"#imgId\").attr(\'src\',path);\n```', 20, 'JavaSE', '前端', 1, 22, 1, 0, 0, '2020-04-27 14:55:20', '2020-04-27 14:55:20');
INSERT INTO `tb_blog_info` VALUES (21, 'Blog-layui', '', '基于LayUI的博客系统技术和部署', '# Blog-layui\n\n**技术** \n- **Springboot**\n- **Mybatis-plus**\n- **mysql**\n- **LayUI**\n- **amaze**\n- **javsScript**\n- **ajax**\n- **themleaf**\n- **X-admin**\n- **MEditor**\n- **qcr**\n\n## 部署\n1，修改文件保存位置  \n2，设置thymeleaf 缓存  \n3，修改文件上传大小  \n4，初始化系统qcr 服务器url  \n5，取消注释登录邮件通知\n6，数据源监控密码账号配置\n7，邮件配置\n8，百度统计\n9，接口文档\n\nTencent docker 部署\ndocker ps\ndocker cp /data/docker/tomcat/blog.war 30a7b176d009:/usr/local/tomcat/webapps/\n\n\n## 作者\n\n- 阳沐之的邮箱：17882555101@163.com\n\n## 感谢\n- [sentsin](https://github.com/sentsin/layui)\n- [pandao](https://github.com/pandao/editor.md)\n- [zjhch123](https://github.com/zjhch123/solo-skin-emiya)', 40, '随笔', '笔记', 1, 21, 1, 0, 0, '2020-05-10 16:01:47', '2020-05-10 16:01:48');
INSERT INTO `tb_blog_info` VALUES (22, 'linux下命令导入、导出mysql数据库', '', ' 导入、导出mysql数据库', '**一、导出数据库用mysqldump命令（注意mysql的安装路径，即此命令的路径）：**\n\n1、导出数据和表结构：\n\n```java\nmysqldump -u用户名 -p密码 数据库名 > 数据库名.sql\n\n/usr/local/mysql/bin/  mysqldump -uroot -p abc > abc.sql\n\n敲回车后会提示输入密码\n\n```\n2、只导出表结构\n\n```java\nmysqldump -u用户名 -p密码 -d 数据库名 > 数据库名.sql\n\n/usr/local/mysql/bin/  mysqldump -uroot -p -d abc > abc.sql\n注：/usr/local/mysql/bin/ —> mysql的data目录\n```\n\n**二、导入数据库**\n\n1、首先建空数据库\n\n```java\nmysql>create database abc;\n```\n\n2、导入数据库\n\n方法一：\n\n（1）选择数据库\n\n\n```java\nmysql>use abc;\n```\n\n（2）设置数据库编码\n\n\n```java\nmysql>set names utf8;\n```\n\n（3）导入数据（注意sql文件的路径）\n\n\n```java\nmysql>source /home/abc/abc.sql;\n```\n\n方法二：\n\n```java\nmysql -u用户名 -p密码 数据库名 < 数据库名.sql\n\nmysql -uabc_f -p abc < abc.sql\n\n建议使用第二种方法导入。\n\n注意：有命令行模式，有sql命令\n```\n\n\n\n\n', 24, 'MySQL', '数据库', 1, 2, 1, 0, 0, '2020-05-10 16:03:14', '2020-05-10 16:03:14');

-- ----------------------------
-- Table structure for tb_blog_link
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_link`;
CREATE TABLE `tb_blog_link`  (
  `link_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '友链表主键id',
  `link_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '友链类别 0-友链 1-推荐 2-个人网站',
  `link_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站名称',
  `link_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站链接',
  `link_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站描述',
  `link_rank` int(11) NOT NULL DEFAULT 0 COMMENT '用于列表排序',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0-未删除 1-已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`link_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '友情链接信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_link
-- ----------------------------
INSERT INTO `tb_blog_link` VALUES (1, 0, 'DeTechn Blog', 'https://www.detechn.com/', 'DeTechn Blog', 5, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_link` VALUES (2, 0, 'tyCoding Blog', 'https://tycoding.cn/', 'tyCoding Blog', 4, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_link` VALUES (4, 1, '芋道源码一\r\n纯源码解析博客', 'http://www.iocoder.cn/', '芋道源码一\r\n纯源码解析博客', 3, 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_link` VALUES (5, 1, '程序猿DD', 'http://blog.didispace.com/', '程序猿DD', 2, 0, '2020-04-28 09:18:49');
INSERT INTO `tb_blog_link` VALUES (6, 1, '犬小哈教程网', 'https://www.exception.site/', '犬小哈教程网', 1, 0, '2020-03-08 01:50:28');
INSERT INTO `tb_blog_link` VALUES (7, 2, 'CSDN', 'https://blog.csdn.net/qq_35385687', '阳沐之的个人博客', 1, 0, '2020-04-28 09:20:13');
INSERT INTO `tb_blog_link` VALUES (8, 2, '2048', 'http://122.51.85.120:99/', '2048', 2, 0, '2020-04-28 12:45:17');

-- ----------------------------
-- Table structure for tb_blog_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_log`;
CREATE TABLE `tb_blog_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '时间',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `userId` int(5) NULL DEFAULT NULL COMMENT '用户id',
  `count` int(15) NULL DEFAULT NULL COMMENT '累计访问量',
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问者ip',
  `locked` tinyint(2) NULL DEFAULT 0 COMMENT '0 正常，1 禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2630 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客日志信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_log
-- ----------------------------
INSERT INTO `tb_blog_log` VALUES (39, '2020-03-08 07:01:46', 'visit', NULL, 1, '192.168.252.1', 0);
INSERT INTO `tb_blog_log` VALUES (307, '2020-04-09 07:00:09', 'delete', 107, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (308, '2020-04-09 07:00:15', 'delete', 107, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (309, '2020-04-09 07:00:41', 'delete', 107, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (310, '2020-04-09 07:01:08', 'delete', 107, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (311, '2020-04-09 07:02:19', 'delete', 107, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (313, '2020-04-09 07:38:54', 'delete', 107, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (347, '2020-04-12 03:18:00', 'visit', NULL, 1, '223.87.240.161', 0);
INSERT INTO `tb_blog_log` VALUES (348, '2020-04-12 03:18:19', 'visit', NULL, 1, '223.87.240.161', 0);
INSERT INTO `tb_blog_log` VALUES (349, '2020-04-12 03:18:26', 'visit', NULL, 1, '223.87.240.161', 0);
INSERT INTO `tb_blog_log` VALUES (350, '2020-04-12 03:19:48', 'visit', NULL, 1, '223.87.240.161', 0);
INSERT INTO `tb_blog_log` VALUES (351, '2020-04-12 03:20:44', 'visit', NULL, 1, '223.87.240.161', 0);
INSERT INTO `tb_blog_log` VALUES (352, '2020-04-12 03:21:38', 'visit', NULL, 1, '113.96.219.248', 0);
INSERT INTO `tb_blog_log` VALUES (353, '2020-04-12 03:22:13', 'visit', NULL, 1, '157.255.246.25', 0);
INSERT INTO `tb_blog_log` VALUES (354, '2020-04-12 03:22:15', 'visit', NULL, 1, '157.255.193.24', 0);
INSERT INTO `tb_blog_log` VALUES (355, '2020-04-12 03:22:15', 'visit', NULL, 1, '157.255.172.14', 0);
INSERT INTO `tb_blog_log` VALUES (356, '2020-04-12 03:22:15', 'visit', NULL, 1, '157.255.172.17', 0);
INSERT INTO `tb_blog_log` VALUES (357, '2020-04-12 03:22:15', 'visit', NULL, 1, '157.255.172.25', 0);
INSERT INTO `tb_blog_log` VALUES (358, '2020-04-12 03:22:15', 'visit', NULL, 1, '157.255.172.126', 0);
INSERT INTO `tb_blog_log` VALUES (359, '2020-04-12 03:22:15', 'visit', NULL, 1, '112.60.1.79', 0);
INSERT INTO `tb_blog_log` VALUES (360, '2020-04-12 03:22:15', 'visit', NULL, 1, '112.60.1.79', 0);
INSERT INTO `tb_blog_log` VALUES (361, '2020-04-12 03:22:18', 'visit', NULL, 1, '58.251.112.211', 0);
INSERT INTO `tb_blog_log` VALUES (362, '2020-04-12 03:22:18', 'visit', NULL, 1, '157.255.172.25', 0);
INSERT INTO `tb_blog_log` VALUES (363, '2020-04-12 03:22:18', 'visit', NULL, 1, '157.255.192.118', 0);
INSERT INTO `tb_blog_log` VALUES (364, '2020-04-12 03:22:18', 'visit', NULL, 1, '157.255.172.126', 0);
INSERT INTO `tb_blog_log` VALUES (365, '2020-04-12 03:22:18', 'visit', NULL, 1, '157.255.172.18', 0);
INSERT INTO `tb_blog_log` VALUES (366, '2020-04-12 03:22:18', 'visit', NULL, 1, '112.60.1.64', 0);
INSERT INTO `tb_blog_log` VALUES (367, '2020-04-12 03:22:18', 'visit', NULL, 1, '112.60.1.97', 0);
INSERT INTO `tb_blog_log` VALUES (368, '2020-04-12 03:22:18', 'visit', NULL, 1, '112.60.1.73', 0);
INSERT INTO `tb_blog_log` VALUES (369, '2020-04-12 03:22:20', 'visit', NULL, 1, '120.204.17.71', 0);
INSERT INTO `tb_blog_log` VALUES (370, '2020-04-12 03:24:45', 'visit', NULL, 1, '223.87.240.161', 0);
INSERT INTO `tb_blog_log` VALUES (408, '2020-04-16 09:59:39', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (409, '2020-04-16 10:01:16', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (410, '2020-04-16 10:01:39', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (415, '2020-04-16 12:51:15', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (416, '2020-04-16 12:51:58', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (417, '2020-04-16 12:54:05', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (418, '2020-04-16 12:54:16', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (419, '2020-04-16 12:56:17', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (420, '2020-04-16 12:56:22', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (421, '2020-04-16 12:59:34', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (422, '2020-04-16 13:00:32', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (423, '2020-04-16 13:00:52', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (424, '2020-04-16 13:00:52', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (425, '2020-04-16 13:01:23', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (426, '2020-04-16 13:01:24', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (427, '2020-04-16 13:06:22', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (428, '2020-04-16 13:06:22', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (435, '2020-04-17 01:34:38', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (436, '2020-04-17 01:36:46', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (437, '2020-04-17 01:57:52', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (439, '2020-04-17 02:00:00', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (440, '2020-04-17 02:07:01', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (441, '2020-04-17 02:07:01', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (442, '2020-04-17 02:07:12', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (444, '2020-04-17 02:08:00', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (445, '2020-04-17 02:09:08', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (450, '2020-04-17 02:57:38', 'upload', 174, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (504, '2020-04-18 02:40:09', 'visit', NULL, 1, '192.168.252.1', 0);
INSERT INTO `tb_blog_log` VALUES (507, '2020-04-18 02:42:16', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (508, '2020-04-18 02:42:21', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (509, '2020-04-18 02:42:25', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (510, '2020-04-18 02:42:28', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (511, '2020-04-18 02:42:33', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (512, '2020-04-18 02:42:36', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (513, '2020-04-18 02:42:38', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (514, '2020-04-18 02:42:44', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (515, '2020-04-18 02:42:49', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (516, '2020-04-18 02:42:55', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (517, '2020-04-18 02:42:58', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (518, '2020-04-18 02:43:02', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (519, '2020-04-18 02:43:08', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (520, '2020-04-18 02:43:13', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (521, '2020-04-18 02:43:18', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (522, '2020-04-18 02:43:23', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (523, '2020-04-18 02:43:29', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (524, '2020-04-18 02:43:34', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (525, '2020-04-18 02:43:37', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (526, '2020-04-18 02:43:43', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (527, '2020-04-18 02:44:38', 'visit', NULL, 1, '192.168.252.1', 0);
INSERT INTO `tb_blog_log` VALUES (529, '2020-04-18 03:00:01', 'visit', NULL, 1, '192.168.252.1', 0);
INSERT INTO `tb_blog_log` VALUES (531, '2020-04-18 03:04:00', 'visit', NULL, 1, '192.168.252.1', 0);
INSERT INTO `tb_blog_log` VALUES (532, '2020-04-18 03:04:09', 'visit', NULL, 1, '192.168.252.1', 0);
INSERT INTO `tb_blog_log` VALUES (534, '2020-04-18 04:02:47', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (535, '2020-04-18 04:03:14', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (538, '2020-04-18 04:06:55', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (539, '2020-04-18 04:10:24', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (540, '2020-04-18 04:10:32', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (541, '2020-04-18 04:10:41', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (553, '2020-04-18 07:04:15', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (554, '2020-04-18 07:04:35', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (555, '2020-04-18 07:04:39', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (556, '2020-04-18 07:05:00', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (557, '2020-04-18 07:05:10', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (558, '2020-04-18 07:05:26', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (563, '2020-04-18 07:09:19', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (567, '2020-04-18 07:14:18', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (568, '2020-04-18 07:14:25', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (569, '2020-04-18 07:14:25', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (570, '2020-04-18 07:15:26', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (571, '2020-04-18 07:16:03', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (572, '2020-04-18 07:18:16', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (573, '2020-04-18 07:19:12', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (574, '2020-04-18 07:25:45', 'visit', NULL, 1, '61.151.178.166', 0);
INSERT INTO `tb_blog_log` VALUES (575, '2020-04-18 07:26:46', 'visit', NULL, 1, '61.151.178.180', 0);
INSERT INTO `tb_blog_log` VALUES (579, '2020-04-18 07:57:14', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (580, '2020-04-18 07:57:22', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (581, '2020-04-18 07:57:37', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (582, '2020-04-18 07:57:44', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (585, '2020-04-18 08:22:48', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (586, '2020-04-18 08:22:54', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (587, '2020-04-18 08:32:11', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (588, '2020-04-18 08:32:19', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (610, '2020-04-18 09:13:40', 'login', 174, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (611, '2020-04-18 09:13:40', 'login', 174, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (612, '2020-04-18 09:14:50', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (613, '2020-04-18 09:15:02', 'upload', 174, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (614, '2020-04-18 09:16:00', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (615, '2020-04-18 09:16:09', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (616, '2020-04-18 09:16:34', 'upload', 174, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (624, '2020-04-18 10:44:47', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (625, '2020-04-18 10:44:54', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (636, '2020-04-18 12:33:01', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (637, '2020-04-18 12:33:13', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (638, '2020-04-18 12:33:15', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (641, '2020-04-18 12:34:19', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (643, '2020-04-18 12:46:16', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (644, '2020-04-18 12:46:28', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (645, '2020-04-18 12:46:44', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (646, '2020-04-18 12:46:53', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (647, '2020-04-18 12:47:01', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (648, '2020-04-18 12:47:10', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (650, '2020-04-18 13:22:34', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (653, '2020-04-18 13:31:12', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (654, '2020-04-18 13:32:31', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (655, '2020-04-18 13:34:23', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (656, '2020-04-18 13:35:01', 'visit', NULL, 1, '117.136.63.159', 0);
INSERT INTO `tb_blog_log` VALUES (657, '2020-04-18 13:35:33', 'visit', NULL, 1, '110.184.65.209', 0);
INSERT INTO `tb_blog_log` VALUES (658, '2020-04-18 13:35:54', 'visit', NULL, 1, '223.104.9.194', 0);
INSERT INTO `tb_blog_log` VALUES (659, '2020-04-18 13:35:54', 'visit', NULL, 1, '117.136.63.159', 0);
INSERT INTO `tb_blog_log` VALUES (660, '2020-04-18 13:36:06', 'visit', NULL, 1, '113.57.182.79', 0);
INSERT INTO `tb_blog_log` VALUES (661, '2020-04-18 13:36:09', 'visit', NULL, 1, '117.136.63.159', 0);
INSERT INTO `tb_blog_log` VALUES (662, '2020-04-18 13:36:17', 'visit', NULL, 1, '223.104.9.194', 0);
INSERT INTO `tb_blog_log` VALUES (663, '2020-04-18 13:36:31', 'visit', NULL, 1, '113.57.182.79', 0);
INSERT INTO `tb_blog_log` VALUES (664, '2020-04-18 13:39:54', 'visit', NULL, 1, '14.20.152.81', 0);
INSERT INTO `tb_blog_log` VALUES (665, '2020-04-18 13:39:54', 'visit', NULL, 1, '14.20.152.81', 0);
INSERT INTO `tb_blog_log` VALUES (666, '2020-04-18 13:39:54', 'visit', NULL, 1, '222.209.46.47', 0);
INSERT INTO `tb_blog_log` VALUES (668, '2020-04-18 13:41:53', 'visit', NULL, 1, '117.136.63.188', 0);
INSERT INTO `tb_blog_log` VALUES (669, '2020-04-18 13:42:25', 'visit', NULL, 1, '222.209.46.47', 0);
INSERT INTO `tb_blog_log` VALUES (670, '2020-04-18 13:43:11', 'visit', NULL, 1, '222.209.46.47', 0);
INSERT INTO `tb_blog_log` VALUES (671, '2020-04-18 13:46:50', 'visit', NULL, 1, '14.204.176.26', 0);
INSERT INTO `tb_blog_log` VALUES (672, '2020-04-18 13:46:50', 'visit', NULL, 1, '14.204.176.26', 0);
INSERT INTO `tb_blog_log` VALUES (673, '2020-04-18 13:46:51', 'visit', NULL, 1, '14.204.176.26', 0);
INSERT INTO `tb_blog_log` VALUES (674, '2020-04-18 13:46:51', 'visit', NULL, 1, '14.204.176.26', 0);
INSERT INTO `tb_blog_log` VALUES (675, '2020-04-18 13:46:51', 'visit', NULL, 1, '14.204.176.26', 0);
INSERT INTO `tb_blog_log` VALUES (676, '2020-04-18 13:46:57', 'visit', NULL, 1, '61.151.178.197', 0);
INSERT INTO `tb_blog_log` VALUES (678, '2020-04-18 13:48:13', 'visit', NULL, 1, '110.184.65.209', 0);
INSERT INTO `tb_blog_log` VALUES (679, '2020-04-18 13:48:23', 'visit', NULL, 1, '110.184.65.209', 0);
INSERT INTO `tb_blog_log` VALUES (680, '2020-04-18 13:49:34', 'visit', NULL, 1, '110.184.65.209', 0);
INSERT INTO `tb_blog_log` VALUES (681, '2020-04-18 13:49:55', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (682, '2020-04-18 13:52:44', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (683, '2020-04-18 13:54:25', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (684, '2020-04-18 13:55:00', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (685, '2020-04-18 13:55:01', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (686, '2020-04-18 13:55:15', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (687, '2020-04-18 13:57:15', 'visit', NULL, 1, '117.136.63.165', 0);
INSERT INTO `tb_blog_log` VALUES (688, '2020-04-18 13:57:16', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (689, '2020-04-18 13:57:16', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (690, '2020-04-18 13:57:32', 'visit', NULL, 1, '222.209.46.47', 0);
INSERT INTO `tb_blog_log` VALUES (691, '2020-04-18 13:58:35', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (692, '2020-04-18 13:58:52', 'visit', NULL, 1, '117.136.63.165', 0);
INSERT INTO `tb_blog_log` VALUES (693, '2020-04-18 13:59:31', 'visit', NULL, 1, '117.136.63.165', 0);
INSERT INTO `tb_blog_log` VALUES (694, '2020-04-18 14:01:49', 'visit', NULL, 1, '117.136.63.165', 0);
INSERT INTO `tb_blog_log` VALUES (695, '2020-04-18 14:02:37', 'visit', NULL, 1, '112.45.166.174', 0);
INSERT INTO `tb_blog_log` VALUES (696, '2020-04-18 14:10:41', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (697, '2020-04-18 14:11:01', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (701, '2020-04-18 14:46:00', 'visit', NULL, 1, '110.184.21.23', 0);
INSERT INTO `tb_blog_log` VALUES (702, '2020-04-18 14:51:59', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (703, '2020-04-18 14:52:19', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (704, '2020-04-18 14:52:22', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (705, '2020-04-18 14:54:19', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (706, '2020-04-18 15:04:10', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (707, '2020-04-18 15:04:18', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (709, '2020-04-18 15:06:11', 'visit', NULL, 1, '112.45.169.67', 0);
INSERT INTO `tb_blog_log` VALUES (710, '2020-04-18 15:09:19', 'visit', NULL, 1, '124.14.133.209', 0);
INSERT INTO `tb_blog_log` VALUES (711, '2020-04-18 15:12:55', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (712, '2020-04-18 15:14:03', 'visit', NULL, 1, '101.91.62.65', 0);
INSERT INTO `tb_blog_log` VALUES (713, '2020-04-18 15:14:02', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (714, '2020-04-18 15:14:46', 'visit', NULL, 1, '124.14.133.209', 0);
INSERT INTO `tb_blog_log` VALUES (715, '2020-04-18 15:15:03', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (716, '2020-04-18 15:15:18', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (717, '2020-04-18 15:15:42', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (718, '2020-04-18 15:20:39', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (719, '2020-04-18 15:20:52', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (720, '2020-04-18 15:21:40', 'visit', NULL, 1, '120.204.17.73', 0);
INSERT INTO `tb_blog_log` VALUES (721, '2020-04-18 15:22:02', 'visit', NULL, 1, '124.14.133.209', 0);
INSERT INTO `tb_blog_log` VALUES (722, '2020-04-18 15:22:10', 'visit', NULL, 1, '61.151.178.180', 0);
INSERT INTO `tb_blog_log` VALUES (723, '2020-04-18 15:22:29', 'visit', NULL, 1, '124.14.133.209', 0);
INSERT INTO `tb_blog_log` VALUES (724, '2020-04-18 15:22:40', 'visit', NULL, 1, '124.14.133.209', 0);
INSERT INTO `tb_blog_log` VALUES (725, '2020-04-18 15:23:33', 'visit', NULL, 1, '124.14.133.209', 0);
INSERT INTO `tb_blog_log` VALUES (726, '2020-04-18 15:26:27', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (727, '2020-04-18 15:26:27', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (728, '2020-04-18 15:26:30', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (729, '2020-04-18 15:27:37', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (730, '2020-04-18 16:48:38', 'visit', NULL, 1, '223.104.215.91', 0);
INSERT INTO `tb_blog_log` VALUES (731, '2020-04-18 18:30:25', 'visit', NULL, 1, '101.89.19.140', 0);
INSERT INTO `tb_blog_log` VALUES (732, '2020-04-18 19:15:13', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (733, '2020-04-19 01:08:20', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (734, '2020-04-19 01:14:56', 'visit', NULL, 1, '117.136.63.157', 0);
INSERT INTO `tb_blog_log` VALUES (735, '2020-04-19 04:16:56', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (736, '2020-04-19 04:46:05', 'visit', NULL, 1, '119.4.253.151', 0);
INSERT INTO `tb_blog_log` VALUES (737, '2020-04-19 04:56:50', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (747, '2020-04-19 06:45:08', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (785, '2020-04-19 10:39:07', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (786, '2020-04-19 10:50:35', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (787, '2020-04-19 10:52:59', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (788, '2020-04-19 10:54:11', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (790, '2020-04-19 11:07:20', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (791, '2020-04-19 11:07:29', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (792, '2020-04-19 11:40:32', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (794, '2020-04-19 12:06:51', 'visit', NULL, 1, '110.191.242.107', 0);
INSERT INTO `tb_blog_log` VALUES (796, '2020-04-19 13:48:08', 'visit', NULL, 1, '110.191.242.107', 0);
INSERT INTO `tb_blog_log` VALUES (797, '2020-04-19 18:38:00', 'visit', NULL, 1, '61.241.50.63', 0);
INSERT INTO `tb_blog_log` VALUES (798, '2020-04-19 19:41:58', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (799, '2020-04-20 00:39:22', 'visit', NULL, 1, '58.247.212.239', 0);
INSERT INTO `tb_blog_log` VALUES (800, '2020-04-20 05:19:22', 'visit', NULL, 1, '175.153.161.222', 0);
INSERT INTO `tb_blog_log` VALUES (801, '2020-04-20 06:16:10', 'visit', NULL, 1, '180.97.118.219', 0);
INSERT INTO `tb_blog_log` VALUES (802, '2020-04-20 06:19:13', 'visit', NULL, 1, '61.129.6.251', 0);
INSERT INTO `tb_blog_log` VALUES (803, '2020-04-20 07:04:39', 'visit', NULL, 1, '59.36.132.240', 0);
INSERT INTO `tb_blog_log` VALUES (804, '2020-04-20 11:50:23', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (806, '2020-04-20 13:00:54', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (807, '2020-04-20 13:05:50', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (808, '2020-04-20 22:55:34', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (809, '2020-04-21 00:56:53', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (810, '2020-04-21 00:56:58', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (812, '2020-04-21 00:58:06', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (813, '2020-04-21 01:00:03', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (814, '2020-04-21 01:03:18', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (815, '2020-04-21 03:35:37', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (816, '2020-04-21 03:36:12', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (817, '2020-04-21 05:05:25', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (818, '2020-04-21 05:05:28', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (819, '2020-04-21 05:05:29', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (821, '2020-04-21 05:06:27', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (822, '2020-04-21 05:06:53', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (823, '2020-04-21 05:08:57', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (824, '2020-04-21 05:29:44', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (825, '2020-04-21 06:12:26', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (826, '2020-04-21 06:12:38', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (829, '2020-04-21 06:20:52', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (831, '2020-04-21 06:21:19', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (832, '2020-04-21 06:22:19', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (833, '2020-04-21 08:16:56', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (834, '2020-04-21 08:30:25', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (835, '2020-04-21 09:15:51', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (846, '2020-04-21 09:25:57', 'visit', NULL, 1, '125.70.116.64', 0);
INSERT INTO `tb_blog_log` VALUES (852, '2020-04-21 11:23:19', 'visit', NULL, 1, '117.173.132.161', 0);
INSERT INTO `tb_blog_log` VALUES (894, '2020-04-21 18:03:11', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (895, '2020-04-21 19:54:17', 'visit', NULL, 1, '59.36.132.240', 0);
INSERT INTO `tb_blog_log` VALUES (896, '2020-04-21 22:13:51', 'visit', NULL, 1, '61.241.50.63', 0);
INSERT INTO `tb_blog_log` VALUES (897, '2020-04-22 00:38:56', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (924, '2020-04-22 04:47:15', 'visit', NULL, 1, '58.247.212.239', 0);
INSERT INTO `tb_blog_log` VALUES (927, '2020-04-22 05:27:57', 'visit', NULL, 1, '58.247.212.239', 0);
INSERT INTO `tb_blog_log` VALUES (968, '2020-04-23 04:19:55', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (970, '2020-04-23 12:14:43', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (971, '2020-04-23 12:19:11', 'visit', NULL, 1, '183.192.179.16', 0);
INSERT INTO `tb_blog_log` VALUES (1015, '2020-04-27 13:58:58', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1108, '2020-04-28 08:00:12', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1109, '2020-04-28 08:01:25', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1110, '2020-04-28 08:02:16', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1114, '2020-04-28 08:23:38', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1115, '2020-04-28 08:24:05', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1116, '2020-04-28 08:24:14', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1117, '2020-04-28 08:32:14', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1118, '2020-04-28 08:32:19', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1120, '2020-04-28 08:37:41', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1121, '2020-04-28 08:37:49', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1122, '2020-04-28 08:46:23', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1123, '2020-04-28 08:46:25', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1124, '2020-04-28 08:49:32', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1125, '2020-04-28 08:49:35', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1132, '2020-04-28 09:00:09', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1133, '2020-04-28 09:01:53', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1134, '2020-04-28 09:02:34', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1135, '2020-04-28 09:08:24', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1136, '2020-04-28 09:09:28', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1137, '2020-04-28 09:12:13', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1138, '2020-04-28 09:12:39', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1139, '2020-04-28 09:12:56', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1140, '2020-04-28 09:13:33', 'visit', NULL, 1, '223.104.215.2', 0);
INSERT INTO `tb_blog_log` VALUES (1141, '2020-04-28 09:14:36', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1142, '2020-04-28 09:19:47', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1143, '2020-04-28 09:24:39', 'visit', NULL, 1, '101.91.60.104', 0);
INSERT INTO `tb_blog_log` VALUES (1144, '2020-04-28 09:28:15', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1145, '2020-04-28 09:28:31', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1146, '2020-04-28 09:28:45', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1149, '2020-04-28 09:59:26', 'delete', 174, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1154, '2020-04-28 11:46:00', 'upload', 174, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1155, '2020-04-28 11:47:41', 'delete', 174, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1167, '2020-04-28 12:40:46', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1168, '2020-04-28 12:40:46', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1169, '2020-04-28 12:41:28', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1170, '2020-04-28 12:41:38', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1171, '2020-04-28 12:42:01', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1172, '2020-04-28 12:42:12', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1173, '2020-04-28 12:42:17', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1174, '2020-04-28 12:42:38', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1176, '2020-04-28 12:45:29', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1178, '2020-04-28 12:54:11', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1179, '2020-04-28 12:57:08', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1180, '2020-04-28 12:59:56', 'visit', NULL, 1, '47.92.95.244', 0);
INSERT INTO `tb_blog_log` VALUES (1181, '2020-04-28 13:00:10', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1182, '2020-04-28 13:13:45', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1183, '2020-04-28 13:14:10', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1184, '2020-04-28 13:15:33', 'visit', NULL, 1, '59.37.97.42', 0);
INSERT INTO `tb_blog_log` VALUES (1185, '2020-04-28 13:15:52', 'visit', NULL, 1, '59.37.97.42', 0);
INSERT INTO `tb_blog_log` VALUES (1186, '2020-04-28 13:16:10', 'visit', NULL, 1, '59.37.97.42', 0);
INSERT INTO `tb_blog_log` VALUES (1187, '2020-04-28 13:16:22', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1188, '2020-04-28 13:16:22', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1189, '2020-04-28 13:16:40', 'visit', NULL, 1, '59.37.97.42', 0);
INSERT INTO `tb_blog_log` VALUES (1190, '2020-04-28 13:16:48', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1191, '2020-04-28 13:21:15', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1192, '2020-04-28 13:21:30', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1193, '2020-04-28 13:31:24', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1194, '2020-04-28 13:31:32', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1195, '2020-04-28 13:32:56', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1196, '2020-04-28 13:34:52', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1197, '2020-04-28 13:35:24', 'visit', NULL, 1, '59.37.97.38', 0);
INSERT INTO `tb_blog_log` VALUES (1198, '2020-04-28 13:41:24', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1199, '2020-04-28 13:53:15', 'visit', NULL, 1, '117.136.62.16', 0);
INSERT INTO `tb_blog_log` VALUES (1200, '2020-04-28 14:16:54', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1201, '2020-04-28 14:18:55', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1202, '2020-04-28 14:19:34', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1203, '2020-04-28 14:20:36', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1204, '2020-04-28 14:22:43', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1205, '2020-04-28 14:36:28', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1206, '2020-04-28 14:37:06', 'visit', NULL, 1, '36.170.33.185', 0);
INSERT INTO `tb_blog_log` VALUES (1212, '2020-04-29 07:52:54', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1213, '2020-04-29 07:57:47', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1214, '2020-04-29 09:05:22', 'visit', NULL, 1, '58.247.212.239', 0);
INSERT INTO `tb_blog_log` VALUES (1216, '2020-04-30 00:17:30', 'visit', NULL, 1, '223.104.215.69', 0);
INSERT INTO `tb_blog_log` VALUES (1219, '2020-04-30 15:37:26', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1220, '2020-04-30 21:53:10', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1221, '2020-05-01 03:56:28', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1222, '2020-05-01 07:59:20', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1223, '2020-05-01 09:31:14', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1224, '2020-05-01 10:28:25', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1225, '2020-05-01 14:34:52', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1226, '2020-05-02 07:33:38', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1227, '2020-05-02 07:34:27', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1228, '2020-05-02 07:34:41', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1229, '2020-05-02 09:16:24', 'visit', NULL, 1, '116.128.128.41', 0);
INSERT INTO `tb_blog_log` VALUES (1230, '2020-05-02 12:31:33', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1231, '2020-05-02 16:58:38', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1232, '2020-05-02 17:01:43', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1233, '2020-05-03 04:22:52', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1234, '2020-05-03 05:33:03', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1236, '2020-05-03 08:26:41', 'visit', NULL, 1, '59.36.132.240', 0);
INSERT INTO `tb_blog_log` VALUES (1237, '2020-05-03 13:58:52', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1238, '2020-05-03 14:02:50', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1239, '2020-05-03 16:07:46', 'visit', NULL, 1, '36.170.37.80', 0);
INSERT INTO `tb_blog_log` VALUES (1240, '2020-05-03 19:57:39', 'visit', NULL, 1, '61.241.50.63', 0);
INSERT INTO `tb_blog_log` VALUES (1244, '2020-05-04 10:47:09', 'visit', NULL, 1, '61.151.207.186', 0);
INSERT INTO `tb_blog_log` VALUES (1245, '2020-05-04 18:55:15', 'visit', NULL, 1, '59.36.132.240', 0);
INSERT INTO `tb_blog_log` VALUES (1246, '2020-05-04 22:44:18', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1281, '2020-05-05 08:41:02', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1287, '2020-05-05 08:42:13', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1288, '2020-05-05 08:42:24', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1289, '2020-05-05 08:42:26', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1290, '2020-05-05 08:42:49', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1291, '2020-05-05 08:43:51', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1292, '2020-05-05 08:43:52', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1293, '2020-05-05 08:44:01', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1294, '2020-05-05 08:44:01', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1295, '2020-05-05 08:44:41', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1296, '2020-05-05 08:44:49', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1297, '2020-05-05 08:44:54', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1298, '2020-05-05 08:44:56', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1299, '2020-05-05 08:45:44', 'visit', NULL, 1, '192.168.42.20', 0);
INSERT INTO `tb_blog_log` VALUES (1300, '2020-05-05 09:58:33', 'visit', NULL, 1, '101.89.19.140', 0);
INSERT INTO `tb_blog_log` VALUES (1301, '2020-05-06 00:32:34', 'visit', NULL, 1, '223.104.215.177', 0);
INSERT INTO `tb_blog_log` VALUES (1302, '2020-05-06 04:19:32', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1303, '2020-05-06 21:38:13', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1304, '2020-05-08 04:00:05', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1305, '2020-05-08 04:00:06', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1306, '2020-05-08 04:01:52', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1307, '2020-05-08 04:02:01', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1308, '2020-05-08 04:02:36', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1309, '2020-05-08 04:02:38', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1310, '2020-05-08 04:02:38', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1311, '2020-05-08 04:02:38', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1312, '2020-05-08 04:02:38', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1313, '2020-05-08 04:03:00', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1314, '2020-05-08 04:06:56', 'visit', NULL, 1, '223.104.215.177', 0);
INSERT INTO `tb_blog_log` VALUES (1315, '2020-05-08 04:08:42', 'visit', NULL, 1, '223.104.215.177', 0);
INSERT INTO `tb_blog_log` VALUES (1316, '2020-05-08 04:36:59', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1317, '2020-05-08 04:40:14', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1318, '2020-05-08 04:40:37', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1319, '2020-05-08 04:40:41', 'visit', NULL, 1, '222.211.207.129', 0);
INSERT INTO `tb_blog_log` VALUES (1323, '2020-05-08 12:39:23', 'visit', NULL, 1, '223.87.231.46', 0);
INSERT INTO `tb_blog_log` VALUES (1325, '2020-05-08 12:40:10', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1327, '2020-05-08 12:46:07', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1328, '2020-05-08 12:51:28', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1329, '2020-05-08 12:52:16', 'delete', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1330, '2020-05-08 12:52:33', 'upload', 1, NULL, NULL, 0);
INSERT INTO `tb_blog_log` VALUES (1336, '2020-05-08 16:27:41', 'visit', NULL, 1, '223.87.231.46', 0);
INSERT INTO `tb_blog_log` VALUES (1337, '2020-05-08 20:54:36', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1338, '2020-05-08 21:40:24', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1339, '2020-05-08 21:57:22', 'visit', NULL, 1, '14.18.182.223', 0);
INSERT INTO `tb_blog_log` VALUES (1340, '2020-05-09 04:50:37', 'visit', NULL, 1, '223.104.215.177', 0);
INSERT INTO `tb_blog_log` VALUES (1341, '2020-05-09 19:27:11', 'visit', NULL, 1, '61.241.50.63', 0);
INSERT INTO `tb_blog_log` VALUES (1342, '2020-05-09 22:38:05', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (1343, '2020-05-10 02:14:31', 'visit', NULL, 1, '223.87.231.46', 0);
INSERT INTO `tb_blog_log` VALUES (1344, '2020-05-10 15:17:34', 'visit', NULL, 1, '182.254.52.17', 0);
INSERT INTO `tb_blog_log` VALUES (2623, '2020-05-11 08:54:30', 'visit', NULL, 1, '127.0.0.1', 0);
INSERT INTO `tb_blog_log` VALUES (2624, '2020-05-11 09:09:55', 'visit', NULL, 1, '172.18.0.1', 0);
INSERT INTO `tb_blog_log` VALUES (2625, '2020-05-11 09:10:12', 'visit', NULL, 1, '172.18.0.1', 0);
INSERT INTO `tb_blog_log` VALUES (2626, '2020-05-11 09:10:20', 'visit', NULL, 1, '172.18.0.1', 0);
INSERT INTO `tb_blog_log` VALUES (2627, '2020-05-11 09:10:24', 'visit', NULL, 1, '172.18.0.1', 0);
INSERT INTO `tb_blog_log` VALUES (2628, '2020-05-11 09:10:26', 'visit', NULL, 1, '172.18.0.1', 0);
INSERT INTO `tb_blog_log` VALUES (2629, '2020-05-11 09:20:27', 'visit', NULL, 1, '172.18.0.1', 0);

-- ----------------------------
-- Table structure for tb_blog_tag
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tag`;
CREATE TABLE `tb_blog_tag`  (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标签表主键id',
  `tag_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签名称',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0=否 1=是',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博文标签信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_tag
-- ----------------------------
INSERT INTO `tb_blog_tag` VALUES (1, 'tomcat', 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_tag` VALUES (57, 'redis', 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_tag` VALUES (58, 'JavaScript', 0, '2020-03-01 10:01:50');
INSERT INTO `tb_blog_tag` VALUES (141, 'Java', 0, '2020-03-08 08:30:38');
INSERT INTO `tb_blog_tag` VALUES (144, 'IDEA', 0, '2020-04-18 13:51:24');
INSERT INTO `tb_blog_tag` VALUES (145, 'Eclipse', 0, '2020-04-18 13:51:28');
INSERT INTO `tb_blog_tag` VALUES (146, 'shell', 0, '2020-04-18 13:51:34');
INSERT INTO `tb_blog_tag` VALUES (147, 'postman', 0, '2020-04-18 13:51:39');
INSERT INTO `tb_blog_tag` VALUES (148, 'swagger', 0, '2020-04-18 13:51:50');
INSERT INTO `tb_blog_tag` VALUES (150, 'mybatis', 0, '2020-04-18 13:52:09');
INSERT INTO `tb_blog_tag` VALUES (151, '笔记', 0, '2020-04-18 14:19:25');
INSERT INTO `tb_blog_tag` VALUES (152, '学习', 0, '2020-04-18 14:19:36');
INSERT INTO `tb_blog_tag` VALUES (153, '生活', 0, '2020-04-18 14:19:45');
INSERT INTO `tb_blog_tag` VALUES (154, '高阶', 0, '2020-04-18 14:19:50');
INSERT INTO `tb_blog_tag` VALUES (155, '中间件', 0, '2020-04-18 14:20:11');
INSERT INTO `tb_blog_tag` VALUES (156, '微服务', 0, '2020-04-18 14:20:24');
INSERT INTO `tb_blog_tag` VALUES (157, 'jvm', 0, '2020-04-19 05:46:40');
INSERT INTO `tb_blog_tag` VALUES (158, 'Linux', 0, '2020-04-19 05:52:52');
INSERT INTO `tb_blog_tag` VALUES (159, 'JDK', 0, '2020-04-19 05:57:34');
INSERT INTO `tb_blog_tag` VALUES (161, '前端', 0, '2020-04-19 05:59:07');
INSERT INTO `tb_blog_tag` VALUES (162, '数据库', 0, '2020-04-19 05:59:11');
INSERT INTO `tb_blog_tag` VALUES (164, '作品', 0, '2020-04-28 09:15:20');
INSERT INTO `tb_blog_tag` VALUES (166, 'LayUI', 0, '2020-04-28 09:15:51');

-- ----------------------------
-- Table structure for tb_blog_tag_relation
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tag_relation`;
CREATE TABLE `tb_blog_tag_relation`  (
  `relation_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '关系表id',
  `blog_id` bigint(20) NOT NULL COMMENT '博客id',
  `tag_id` int(11) NOT NULL COMMENT '标签id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`relation_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 428 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客与标签关系表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_blog_tag_relation
-- ----------------------------
INSERT INTO `tb_blog_tag_relation` VALUES (389, 13, 1, '2020-04-19 05:53:29');
INSERT INTO `tb_blog_tag_relation` VALUES (390, 12, 158, '2020-04-19 05:53:48');
INSERT INTO `tb_blog_tag_relation` VALUES (391, 8, 158, '2020-04-19 05:54:12');
INSERT INTO `tb_blog_tag_relation` VALUES (392, 11, 158, '2020-04-19 05:54:51');
INSERT INTO `tb_blog_tag_relation` VALUES (393, 14, 1, '2020-04-19 05:55:05');
INSERT INTO `tb_blog_tag_relation` VALUES (394, 19, 157, '2020-04-19 05:55:25');
INSERT INTO `tb_blog_tag_relation` VALUES (395, 15, 151, '2020-04-19 05:55:49');
INSERT INTO `tb_blog_tag_relation` VALUES (396, 16, 154, '2020-04-19 05:56:20');
INSERT INTO `tb_blog_tag_relation` VALUES (397, 17, 151, '2020-04-19 05:56:41');
INSERT INTO `tb_blog_tag_relation` VALUES (398, 18, 141, '2020-04-19 05:57:16');
INSERT INTO `tb_blog_tag_relation` VALUES (400, 20, 161, '2020-04-27 14:55:19');
INSERT INTO `tb_blog_tag_relation` VALUES (406, 1, 153, '2020-04-28 11:58:43');
INSERT INTO `tb_blog_tag_relation` VALUES (407, 1, 152, '2020-04-28 11:58:43');
INSERT INTO `tb_blog_tag_relation` VALUES (416, 21, 151, '2020-05-10 16:01:47');
INSERT INTO `tb_blog_tag_relation` VALUES (417, 22, 162, '2020-05-10 16:03:14');
INSERT INTO `tb_blog_tag_relation` VALUES (427, 7, 57, '2020-05-11 07:51:18');

SET FOREIGN_KEY_CHECKS = 1;
