package com.site.blog;

import com.site.blog.entity.BlogLink;
import com.site.blog.service.BlogLinkService;
import com.site.blog.service.BlogLogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyBlogApplicationTests {

	@Autowired
	private BlogLinkService blogLinkService;

	@Autowired
	private BlogLogService blogLogService;

	@Test
	public void selectChain() {
		List<BlogLink> blogLinkList = blogLinkService.lambdaQuery().lt(BlogLink::getLinkId, 500).list();
		blogLinkList.forEach(System.out::println);
	}
}
