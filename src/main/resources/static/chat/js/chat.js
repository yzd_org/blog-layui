'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');
var userCount = 0;

var stompClient = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
    username = document.querySelector('#name').value.trim();

    if (username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');
        /**
         * 使用STMOP子协议的WebSocket客户端
         *  http://122.51.85.120:80/blog/ws
         * /特别注意链接的IP和浏览器访问地址必须保持一致
         * 不然就会出现链接不上的问题
         */
        var socket = new SockJS('/blog/ws');
        stompClient = Stomp.over(socket);

        var tempCount = sessionStorage.getItem("userCount");
        if (tempCount === '' || tempCount === null || tempCount < 0) {
            sessionStorage.setItem("userCount", userCount);
        }
        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}


function onConnected() {
    // 订阅服务端消息
    stompClient.subscribe('/topic/public', onMessageReceived);

    //浏览器向后台推送消息
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )

    connectingElement.classList.add('hidden');
}


function onError(error) {
    connectingElement.textContent = '不可以连接到服务器，请刷新页面后重试!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    var messageContent = messageInput.value.trim();

    if (messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };
        //浏览器向后台推送消息
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}

//接收处理消息
function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);
    userCount = sessionStorage.getItem("userCount");
    var messageElement = document.createElement('li');
    if (message.type === 'JOIN') {
        userCount++;
        sessionStorage.setItem("userCount", userCount);
        messageElement.classList.add('event-message');
        message.content = message.sender + ' 加入!' + " 当前人数：" + userCount;
    } else if (message.type === 'LEAVE') {
        userCount--;
        sessionStorage.setItem("userCount", userCount);
        messageElement.classList.add('event-message');
        message.content = message.sender + ' 离开!' + " 当前人数：" + userCount;
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

//颜色随机
function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}

//侦听事件
usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)
