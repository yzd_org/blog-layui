package com.site.blog.service;

import com.site.blog.entity.BlogCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**

 * 博客分类 服务类
 * </p>
 *
 * @author: 阳沐之
 * @since 2020-03-30
 */
public interface BlogCategoryService extends IService<BlogCategory> {

}
