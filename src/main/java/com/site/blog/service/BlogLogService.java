package com.site.blog.service;

import com.site.blog.entity.BlogLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yzd
 * @since 2020-03-07
 */
public interface BlogLogService extends IService<BlogLog> {

}
