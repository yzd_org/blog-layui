package com.site.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.site.blog.entity.BlogFile;

import java.util.List;

/**
 * (BlogFile)表服务接口
 */
public interface BlogFileService extends IService<BlogFile> {

    /**
     * 通过ID查询单条数据
     */
    BlogFile queryById(Integer id);

    /**
     * 查询多条数据
     */
    List<BlogFile> queryAllByLimit();

    /**
     * 新增
     */
    BlogFile insert(BlogFile BlogFile);

    /**
     * 修改数据
     */
    Integer update(BlogFile BlogFile);

    /**
     * 删除数据
     */
    boolean deleteById(Integer id);

}