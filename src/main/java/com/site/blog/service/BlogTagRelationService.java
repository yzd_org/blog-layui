package com.site.blog.service;

import com.site.blog.entity.BlogInfo;
import com.site.blog.entity.BlogTagRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 博客跟标签的关系表
 */
public interface BlogTagRelationService extends IService<BlogTagRelation> {

    /**
     * 移除本来的标签保存新标签
     */
    void removeAndSaveBatch(List<Integer> blogTagIds, BlogInfo blogInfo);

}
