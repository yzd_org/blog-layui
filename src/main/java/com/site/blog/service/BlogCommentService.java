package com.site.blog.service;

import com.site.blog.entity.BlogComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**

 * 评论信息表 服务类
 * </p>
 *
 * @author: 阳沐之
 * @since 2020-03-04
 */
public interface BlogCommentService extends IService<BlogComment> {

}
