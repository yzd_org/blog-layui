package com.site.blog.service;

import com.site.blog.controller.vo.SimpleBlogListVO;
import com.site.blog.entity.BlogInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 博客信息表 服务类
 */
public interface BlogInfoService extends IService<BlogInfo> {

    /**
     * 返回最新的五条文章列表
     */
    List<SimpleBlogListVO> getNewBlog();

    /**
     * 返回点击量最多的五条文章
     */
    List<SimpleBlogListVO> getHotBlog();

    /**
     * 删除文章
     */
    boolean clearBlogInfo(Long blogId);

}
