package com.site.blog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.site.blog.dao.FileMapper;
import com.site.blog.entity.BlogFile;
import com.site.blog.service.BlogFileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (BlogFile)表服务实现类
 *
 * @author YZD
 * @since 2020-03-07 15:42:45
 */
@Service("BlogFileService")
public class BlogFileServiceImpl extends ServiceImpl<FileMapper, BlogFile> implements BlogFileService {
    @Resource
    private FileMapper fileMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public BlogFile queryById(Integer id) {
        return fileMapper.selectById(id);
    }

    /**
     * 查询多条数据
     *
     * @return 对象列表
     */
    @Override
    public List<BlogFile> queryAllByLimit() {
        return fileMapper.selectList(null);
    }

    /**
     * 新增数据
     *
     * @param BlogFile 实例对象
     * @return 实例对象
     */
    @Override
    public BlogFile insert(BlogFile BlogFile) {
        fileMapper.insert(BlogFile);
        return BlogFile;
    }

    /**
     * 修改数据
     *
     * @param BlogFile 实例对象
     * @return 实例对象
     */
    @Override
    public Integer update(BlogFile BlogFile) {
        fileMapper.updateById(BlogFile);
        return BlogFile.getId();
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return fileMapper.deleteById(id) > 0;
    }
}