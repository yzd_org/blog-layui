package com.site.blog.service.impl;

import com.site.blog.entity.BlogComment;
import com.site.blog.dao.BlogCommentMapper;
import com.site.blog.service.BlogCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**

 * 评论信息表 服务实现类
 * </p>
 *
 * @author: 阳沐之
 * @since 2020-03-04
 */
@Service
public class BlogCommentServiceImpl extends ServiceImpl<BlogCommentMapper, BlogComment> implements BlogCommentService {

}
