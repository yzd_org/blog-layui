package com.site.blog.service.impl;

import com.site.blog.entity.BlogLog;
import com.site.blog.dao.TbBlogLogMapper;
import com.site.blog.service.BlogLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yzd
 * @since 2020-03-07
 */
@Service
public class BlogLogServiceImpl extends ServiceImpl<TbBlogLogMapper, BlogLog> implements BlogLogService {

}
