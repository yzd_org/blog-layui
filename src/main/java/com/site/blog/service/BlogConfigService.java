package com.site.blog.service;

import com.site.blog.entity.BlogConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**

 *  服务类
 * </p>
 *
 * @author: 阳沐之
 * @since 2020-03-3
 */
public interface BlogConfigService extends IService<BlogConfig> {

    Map<String, String> getAllConfigs();

}
