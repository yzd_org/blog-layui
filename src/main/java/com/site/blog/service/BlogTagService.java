package com.site.blog.service;

import com.site.blog.entity.BlogTag;
import com.baomidou.mybatisplus.extension.service.IService;
import com.site.blog.entity.BlogTagCount;

import java.util.List;

/**
 * 标签表 服务类
 * @author yzd
 */
public interface BlogTagService extends IService<BlogTag> {


    /**
     * 最热标签-博文条数
     *
     * @return
     */
    List<BlogTagCount> getBlogTagCountForIndex();

}
