package com.site.blog.service;

import com.site.blog.entity.BlogLink;
import com.baomidou.mybatisplus.extension.service.IService;

/**

 * 友情链接表 服务类
 * </p>
 *
 * @author: 阳沐之
 * @since 2020-03-02
 */
public interface BlogLinkService extends IService<BlogLink> {

}
