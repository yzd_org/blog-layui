package com.site.blog.service;

import javax.mail.MessagingException;


public interface MailService {


    /**
     * 发送文本邮件
     * @param to
     * @param subject
     * @param content
     */
    void sendSimpleMail(String to, String subject, String content) throws MessagingException;

    /**
     * 发送html邮件
     * @param to
     * @param subject
     * @param content
     */
    void sendHtmlMail(String to, String subject, String content) throws MessagingException;

    /**
     * 发送附件
     * @param to
     * @param subject
     * @param content
     * @param filepath
     */
    void sendAttachmentMail(String to, String subject, String content, String filepath) throws MessagingException;

    /**
     * 发送图片
     * @param to
     * @param subject
     * @param content
     * @param rscPath
     * @param rscId
     */
    void sendImageMail(String to, String subject, String content, String rscPath, String rscId);
}