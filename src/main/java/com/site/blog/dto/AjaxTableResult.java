package com.site.blog.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页表格数据返回
 **/
@Data
@ApiModel(description = "分页表格数据返回")
public class AjaxTableResult<T> implements Serializable {

    private static final long serialVersionUID = 3703126015507564836L;
    //状态码
    private int statusCode;

    //提示消息
    private String msg;

    //总条数
    private long count;

    //表格数据
    private List<T> data;

}
