package com.site.blog.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页表格数据返回
 **/
@Data
@ApiModel(description = "分页表格数据返回")
public class AjaxResultPage<T> implements Serializable {

    private static final long serialVersionUID = -3298153384288690279L;
    //状态码
    private int code;

    //提示消息
    private String msg;

    //总条数
    private long count;

    //表格数据
    private List<T> data;

}
