package com.site.blog.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;

/**
 * 分页查询[带条件]输入映射
 **/
@ToString
@ApiModel(description = "分页查询")
public class AjaxPutPage<T> {

    //当前页码
    @Value("1")
    Integer page;

    //每页显示
    @Value("10")
    Integer limit;

    //从多少开始
    @Value("1")
    Integer start;

    //条件类
    T condition;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
        this.start = (this.page - 1) * this.limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public T getCondition() {
        return condition;
    }

    public void setCondition(T condition) {
        this.condition = condition;
    }

    /**
     * 将符合Layui的格式转成mybtais-plus分页的page
     *
     * @return
     */
    public Page<T> putPageToPage() {
        return new Page<T>(this.page, this.limit);
    }

}
