package com.site.blog.dto;

import com.site.blog.constants.HttpStatusConstants;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * 返回结果集
 */
@ApiModel(description = "返回结果集")
public class Result<T> implements Serializable {
    private static final long serialVersionUID = -1654428247767245752L;
    private int code;
    private String message;
    private T data;

    public Result() {
    }

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Result(T t) {
        this.setCode(HttpStatusConstants.OK.getStatus());
        this.setMessage(HttpStatusConstants.OK.getContent());
        this.setData(t);
    }


    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
