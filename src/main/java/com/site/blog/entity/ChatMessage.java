package com.site.blog.entity;

/**
 * 消息类型、发送者、内容
 */
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;

    /**
     * 消息类型定义：chat、jion、leave
     */
    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
