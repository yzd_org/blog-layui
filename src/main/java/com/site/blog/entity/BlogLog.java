package com.site.blog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author yzd
 * @since 2020-03-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("日志")
public class BlogLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 时间
     */
    @TableField("date")
    private Date date;

    /**
     * 描述
     */
    @TableField("description")
    private String description;

    /**
     * 累计访问量
     */
    @TableField("count")
    private Integer count;

    /**
     * 访问者ip
     */
    @TableField("ip")
    private String ip;

    /**
     * 0 正常，1 禁用
     */
    @TableField("locked")
    private Integer locked;

    @TableField("userId")
    private Integer userId;


}
