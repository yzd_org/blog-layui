package com.site.blog.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 阳沐之
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("标签下博文信息")
public class BlogTagCount {
    private Integer tagId;

    private String tagName;

    private long tagCount;



}
