package com.site.blog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author YZD
 * @since 2020-03-07 15:34:17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("文件信息")
public class BlogFile implements Serializable {
    private static final long serialVersionUID = 895498771670770169L;
    /**
     * 文件id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 文件名称
     */
    @TableField("name")
    private String name;
    /**
     * 文件url
     */
    @TableField("url")
    private String url;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 文件类型，01：视频，02：图片，03：普通文件
     */
    @TableField("type")
    private Integer type;

}