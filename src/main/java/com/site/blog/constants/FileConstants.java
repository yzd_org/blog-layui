package com.site.blog.constants;

/**
 * 文件类型
 */
public enum FileConstants {
    video(1, "视频"),
    image(2, "图片"),
    file(3, "普通文件"),
    qcr(4, "系统二维码"),
    ;

    private final int type;
    private final String description;

    public int getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    FileConstants(int type, String description) {
        this.type = type;
        this.description = description;
    }
}
