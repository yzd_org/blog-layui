package com.site.blog.constants;

/**
 * @Description: 主题
 * @author: YZD
 * @Date: 2020-03-08 01:22
 * @Version: 1.0
 */
public enum themConstants {
    amaze("amaze");

    private final String theme;

    themConstants(String theme) {
        this.theme = theme;
    }
}
