package com.site.blog.constants;

/**
 * @Description: 日志
 * @author: YZD
 * @Date: 2020-03-07 22:32
 * @Version: 1.0
 */
public enum LogConstants {
    login("login"),
    visit("visit"),
    logout("logout"),
    delete("delete"),
    uoload("upload"),
    ;

    private final String description;

    public String getDescription() {
        return description;
    }

    LogConstants(String description) {
        this.description = description;
    }
}
