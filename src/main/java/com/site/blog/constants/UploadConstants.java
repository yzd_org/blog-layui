package com.site.blog.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 图片上传路径常量
 */
@Component
public class UploadConstants {
    /**
     * 根路径 文件路径
     */
    @Value("${file.root}")
    public static String ROOT = "/tmp";
    /**
     * 用户头像实际上传路径
     */
    public static String UPLOAD_AUTHOR_IMG = ROOT + "/myblog/authorImg/";
    /**
     * 用户头像数据库路径 用于映射
     */
    public static String SQL_AUTHOR_IMG = "/authorImg/";
    /**
     * 文章图片实际上传路径
     */
    public static String FILE_UPLOAD_DIC = ROOT + "/myblog/";
    /**
     * 文章图片数据库路径  用于映射
     */
    public static String FILE_SQL_DIC = "/upload/";
    /**
     * QCR 实际路径
     */
    public static String QCR_IMAGES = FILE_UPLOAD_DIC + "QCR.png";
    /**
     * 自定义文件上传实际路径
     */
    public static String UPLOAD = FILE_UPLOAD_DIC + FILE_SQL_DIC;
    /**
     * 数据库 自定义文件上传路径  用于映射
     */
    public static String MYFILE = "/myfile/";
    /**
     * 系统日志
     */
    @Value("${logging.file}")
    public static String SYSLOG = "./blog/";
}
