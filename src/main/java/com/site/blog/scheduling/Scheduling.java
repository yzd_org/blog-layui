package com.site.blog.scheduling;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.site.blog.entity.BlogLog;
import com.site.blog.response.DayVisit;
import com.site.blog.service.BlogLogService;
import com.site.blog.service.MailService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.SystemUsageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author yzd
 */
@Component
public class Scheduling {
    @Resource
    MailService mailService;

    @Autowired
    private BlogLogService blogLogService;

    @Value("${spring.mail.username}")
    private String mailTo;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    //    @Scheduled(cron = "0/2 * * * * ?")
    @Scheduled(cron = "0 0 0 * * ? ")
    public void process() throws MessagingException, UnknownHostException, ParseException {
        logger.info("定时发送系统信息到邮件");
        StringBuffer result = new StringBuffer();
        long totalMemory = Runtime.getRuntime().totalMemory();
        result.append("主机: ").append(InetAddress.getLocalHost().getHostName()).append("\n");
        result.append("IP： ").append(InetAddress.getLocalHost().getHostAddress()).append("\n");
        result.append("时间: ").append(LocalDateTime.now()).append("\n");
        result.append("blog使用的总内存为：").append(totalMemory / (1024 * 1024)).append("MB").append("\n");
        result.append("CPU使用量为：").append(SystemUsageUtil.getCpuUsage() * 100).append("%").append("\n");
        result.append("内存使用量为：").append(SystemUsageUtil.getMemoryUsage() * 100).append("%").append("\n");
        result.append("blog今日访问量为：").append(this.today()).append("\n");
        mailService.sendSimpleMail(mailTo, "博客系统运行情况", result.toString());
    }

    public int today() throws ParseException {
        Date pastDate = DateUtils.getPastDate(1);
        List<BlogLog> blogList = blogLogService.list(new QueryWrapper<BlogLog>().lambda().ge(BlogLog::getDate, pastDate));
        SortedMap<Date, List<DayVisit>> sort = dateCount(blogList);
        List<Integer> result = new ArrayList<>();
        for (Map.Entry<Date, List<DayVisit>> stringListEntry : sort.entrySet()) {
            int count = stringListEntry.getValue().size();
            result.add(count);
        }
        return result.get(result.size() - 1);
    }

    public static SortedMap<Date, List<DayVisit>> dateCount(List<BlogLog> blogList) throws ParseException {
        List<DayVisit> dayVisit = new ArrayList<>();
        for (BlogLog blogLog : blogList) {
            Date date = DateUtils.dateFormat2(blogLog.getDate());
            DayVisit visit = new DayVisit();
            visit.setDate(date);
            dayVisit.add(visit);
        }
        Map<Date, List<DayVisit>> collect = dayVisit.stream().collect(Collectors.groupingBy(DayVisit::getDate));
        SortedMap<Date, List<DayVisit>> sort = new TreeMap<>(collect);
        return sort;
    }

}
