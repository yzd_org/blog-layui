package com.site.blog.controller.common;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.entity.BlogInfo;
import com.site.blog.service.BlogInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@Api(tags = "我的归档")
public class Logs {

    @Autowired
    private BlogInfoService blogInfoService;

    @ApiOperation("归档数据+页面")
    @GetMapping("/logs")
    public String logs(HttpServletRequest request) {
        List<BlogInfo> blogInfos = blogInfoService.list(new QueryWrapper<BlogInfo>().lambda()
                .eq(BlogInfo::getBlogStatus, BlogStatusConstants.ONE)
                .eq(BlogInfo::getIsDeleted, BlogStatusConstants.ZERO));
        blogInfos.stream().forEach(item -> {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String date = format.format(item.getCreateTime());
            try {
                item.setCreateTime(format.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        Map<Date, List<BlogInfo>> dateListMap = blogInfos.stream().collect(Collectors.groupingBy(BlogInfo::getCreateTime));
        SortedMap<Date, List<BlogInfo>> sortedMap = new TreeMap<>(dateListMap);
        List<com.site.blog.controller.vo.Logs> logs = new ArrayList<>();
        for (Map.Entry<Date, List<BlogInfo>> dateListEntry : sortedMap.entrySet()) {
            com.site.blog.controller.vo.Logs log = new com.site.blog.controller.vo.Logs();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateListEntry.getKey();
            log.setDate(format.format(date));
            Map<Long, String> titles = dateListEntry.getValue().stream()
                    .collect(Collectors.toMap(BlogInfo::getBlogId, BlogInfo::getBlogPreface));
            log.setTitles(titles);
            logs.add(log);
        }
        Collections.reverse(logs);
        request.setAttribute("logs", logs);
        return "adminLayui/logs";
    }
}
