package com.site.blog.controller.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author 阳沐之
 */
@Data
@ApiModel("博客详情")
public class BlogDetailVO {
    private Long blogId;

    private String blogTitle;

    private Integer blogCategoryId;

    private Integer commentCount;

    private String blogCategoryIcon;

    private String blogCategoryName;

    private String blogCoverImage;

    private Long blogViews;

    private List<String> blogTags;

    private String blogContent;

    private Integer enableComment;

    private Date createTime;

    public Long getBlogId() {
        return blogId;
    }

}
