package com.site.blog.controller.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@ApiModel("归档vo")
public class Logs implements Serializable {
    private static final long serialVersionUID = 1310486673747571470L;

    private String date;

    private Map<Long, String> titles;
}
