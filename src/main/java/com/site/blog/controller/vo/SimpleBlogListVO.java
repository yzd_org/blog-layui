package com.site.blog.controller.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 阳沐之
 */
@Data
@ApiModel("用户返回最新-最热文章")
public class SimpleBlogListVO implements Serializable {

    private static final long serialVersionUID = 3288507617102261009L;
    private Long blogId;

    private String blogTitle;

}
