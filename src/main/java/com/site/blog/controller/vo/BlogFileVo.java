package com.site.blog.controller.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("博客文件")
public class BlogFileVo implements Serializable {
    private static final long serialVersionUID = 895498771670770169L;
    /**
     * 文件id
     */
    private Integer id;
    /**
     * 文件名称
     */
    private String name;
    /**
     * 文件url
     */
    private String url;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 文件类型，01：视频，02：图片，03：普通文件
     */
    private String type;

}