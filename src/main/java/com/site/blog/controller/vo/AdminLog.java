package com.site.blog.controller.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author yzd
 * @since 2020-03-07
 */
@Data
@ApiModel("访问控制")
public class AdminLog implements Serializable {

    private static final long serialVersionUID = 1899553648431899824L;

    private Integer id;
    /**
     * 时间
     */
    private Date date;
    /**
     * 描述
     */
    private String description;
    /**
     * 访问者ip
     */
    private String ip;

}
