package com.site.blog.controller.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("日访问量VO")
public class DayVo {

    String day;

    Integer count;
}
