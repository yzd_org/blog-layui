package com.site.blog.controller.blog;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.constants.themConstants;
import com.site.blog.entity.BlogInfo;
import com.site.blog.service.BlogConfigService;
import com.site.blog.service.BlogInfoService;
import com.site.blog.service.BlogTagService;
import com.site.blog.util.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;


/**
 * @author yzd
 */
@Controller
@Api(tags = "分类查询相关")
public class MyCategoryController {

    public static themConstants theme = themConstants.amaze;
    @Autowired
    private BlogTagService blogTagService;

    @Autowired
    private BlogInfoService blogInfoService;

    @Autowired
    private BlogConfigService blogConfigService;


    @ApiOperation("根据分类名称查询博文")
    @GetMapping({"/category/{categoryName}"})
    public String category(HttpServletRequest request, @PathVariable("categoryName") String categoryName) {
        return category(request, categoryName, 1);
    }

    /**
     * @param request
     * @param categoryName
     * @param pageNum
     */
    @ApiOperation("博文-分类列表")
    @GetMapping({"/category/{categoryName}/{pageNum}"})
    public String category(HttpServletRequest request, @PathVariable("categoryName") String categoryName, @PathVariable("pageNum") Integer pageNum) {
        Page<BlogInfo> page = new Page<BlogInfo>(pageNum, 8);
        blogInfoService.page(page, new QueryWrapper<BlogInfo>()
                .lambda()
                .eq(BlogInfo::getBlogStatus, BlogStatusConstants.ONE)
                .eq(BlogInfo::getIsDeleted, BlogStatusConstants.ZERO)
                .eq(BlogInfo::getBlogCategoryName, categoryName)
                .orderByDesc(BlogInfo::getCreateTime));
        PageResult blogPageResult = new PageResult
                (page.getRecords(), page.getTotal(), 8, pageNum);

        request.setAttribute("blogPageResult", blogPageResult);
        request.setAttribute("pageName", "分类");
        request.setAttribute("pageUrl", "category");
        request.setAttribute("keyword", categoryName);
        request.setAttribute("newBlogs", blogInfoService.getNewBlog());
        request.setAttribute("hotBlogs", blogInfoService.getHotBlog());
        request.setAttribute("hotTags", blogTagService.getBlogTagCountForIndex());
        request.setAttribute("configurations", blogConfigService.getAllConfigs());
        return "blog/" + theme + "/list";
    }
}
