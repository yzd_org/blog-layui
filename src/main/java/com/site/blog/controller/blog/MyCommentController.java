package com.site.blog.controller.blog;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.constants.SysConfigConstants;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogComment;
import com.site.blog.entity.BlogConfig;
import com.site.blog.entity.BlogInfo;
import com.site.blog.service.BlogCommentService;
import com.site.blog.service.BlogConfigService;
import com.site.blog.service.BlogInfoService;
import com.site.blog.service.MailService;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author yzd
 */
@Controller
@Api(tags = "博文评论相关")
public class MyCommentController {

    @Resource
    MailService mailService;
    @Value("${spring.mail.username}")
    private String mailTo;

    @Autowired
    private BlogCommentService blogCommentService;

    @Autowired
    private BlogInfoService blogInfoService;

    @Autowired
    private BlogConfigService blogConfigService;

    @ApiOperation("根据博客id查询评论列表")
    @GetMapping("/listComment")
    @ResponseBody
    public AjaxResultPage<BlogComment> listComment(AjaxPutPage<BlogComment> ajaxPutPage, Integer blogId) {
        Page<BlogComment> page = ajaxPutPage.putPageToPage();
        blogCommentService.page(page, new QueryWrapper<BlogComment>()
                .lambda()
                .eq(BlogComment::getBlogId, blogId)
                .eq(BlogComment::getCommentStatus, BlogStatusConstants.ONE)
                .eq(BlogComment::getIsDeleted, BlogStatusConstants.ZERO)
                .orderByDesc(BlogComment::getCommentCreateTime));
        AjaxResultPage<BlogComment> ajaxResultPage = new AjaxResultPage<>();

        BlogConfig img = blogConfigService.getOne(new QueryWrapper<BlogConfig>().lambda()
                .eq(BlogConfig::getConfigField, SysConfigConstants.SYS_AUTHOR_IMG.getConfigField()).select(BlogConfig::getConfigValue));
        List<BlogComment> records = page.getRecords();
        if (!CollectionUtils.isEmpty(records)) {
            for (BlogComment record : records) {
                record.setImg(img.getConfigValue());
            }
        }
        ajaxResultPage.setCount(page.getTotal());
        ajaxResultPage.setData(records);
        return ajaxResultPage;
    }

    @ApiOperation("提交评论")
    @PostMapping(value = "/comment")
    @ResponseBody
    public Result comment(HttpServletRequest request,
                          @Validated BlogComment blogComment) throws MessagingException {
        String ref = request.getHeader("Referer");
        if (StringUtils.isEmpty(ref)) {
            return ResponseEntity.failMessage("非法请求");
        }
        boolean flag = blogCommentService.save(blogComment);
        if (flag) {
            //  邮件评论通知
            BlogInfo blogInfo = blogInfoService.getOne(new QueryWrapper<BlogInfo>().lambda().eq(BlogInfo::getBlogId, blogComment.getBlogId()));
            if (blogInfo.getEmailComment() == BlogStatusConstants.ONE) {
                StringBuffer commentResult = new StringBuffer();
                commentResult.append("评论内容: ").append(blogComment.getCommentBody()).append("\n");
                commentResult.append("评论时间: ").append(DateUtil.formatDateTime(new Date())).append("\n");
                commentResult.append("评论人邮箱： ").append(blogComment.getEmail()).append("\n");
                mailService.sendSimpleMail(mailTo, String.format("您的博客：%s，被 %s 评论了，快去回复他吧！",
                        blogInfo.getBlogTitle(), blogComment.getCommentator()), commentResult.toString());
            }
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

}
