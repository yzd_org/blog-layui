package com.site.blog.controller.blog;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.constants.themConstants;
import com.site.blog.entity.BlogInfo;
import com.site.blog.service.BlogConfigService;
import com.site.blog.service.BlogInfoService;
import com.site.blog.service.BlogTagService;
import com.site.blog.util.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description:
 * @author: YZD
 * @Date: 2020-03-08 01:27
 * @Version: 1.0
 */
@Controller
@Api(tags = "文章搜索")
public class MySearchController {

    public static themConstants theme = themConstants.amaze;
    @Autowired
    private BlogInfoService blogInfoService;
    @Autowired
    private BlogTagService blogTagService;
    @Autowired
    private BlogConfigService blogConfigService;

    @ApiOperation("根据名称搜索")
    @GetMapping({"/search/{keyword}"})
    public String search(HttpServletRequest request, @PathVariable("keyword") String keyword) {
        return search(request, keyword, 1);
    }

    @ApiOperation("根据名称分页搜索")
    @GetMapping({"/search/{keyword}/{pageNum}"})
    public String search(HttpServletRequest request, @PathVariable("keyword") String keyword, @PathVariable("pageNum") Integer pageNum) {

        Page<BlogInfo> page = new Page<>(pageNum, 8);
        blogInfoService.page(page, new QueryWrapper<BlogInfo>()
                .lambda().like(BlogInfo::getBlogTitle, keyword).or()
                .like(!StringUtils.isEmpty(keyword), BlogInfo::getBlogContent, keyword)
                .eq(BlogInfo::getBlogStatus, BlogStatusConstants.ONE)
                .eq(BlogInfo::getIsDeleted, BlogStatusConstants.ZERO)
                .orderByDesc(BlogInfo::getCreateTime));
        PageResult blogPageResult = new PageResult
                (page.getRecords(), page.getTotal(), 8, pageNum);

        request.setAttribute("blogPageResult", blogPageResult);
        request.setAttribute("pageName", "搜索");
        request.setAttribute("pageUrl", "search");
        request.setAttribute("keyword", keyword);
        request.setAttribute("newBlogs", blogInfoService.getNewBlog());
        request.setAttribute("hotBlogs", blogInfoService.getHotBlog());
        request.setAttribute("hotTags", blogTagService.getBlogTagCountForIndex());
        request.setAttribute("configurations", blogConfigService.getAllConfigs());
        return "blog/" + theme + "/list";
    }

}
