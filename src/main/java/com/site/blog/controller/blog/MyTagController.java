package com.site.blog.controller.blog;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.constants.themConstants;
import com.site.blog.entity.BlogInfo;
import com.site.blog.entity.BlogTagRelation;
import com.site.blog.service.BlogConfigService;
import com.site.blog.service.BlogInfoService;
import com.site.blog.service.BlogTagRelationService;
import com.site.blog.service.BlogTagService;
import com.site.blog.util.PageResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author yzd
 */
@Controller
@Api(tags = "标签查询相关")
public class MyTagController {

    public static themConstants theme = themConstants.amaze;
    @Autowired
    private BlogTagService blogTagService;

    @Autowired
    private BlogTagRelationService blogTagRelationService;

    @Autowired
    private BlogInfoService blogInfoService;

    @Autowired
    private BlogConfigService blogConfigService;

    /**
     * @param request
     * @param tagId
     */
    @ApiOperation("根据标签id查询")
    @GetMapping({"/tag/{tagId}"})
    public String tag(HttpServletRequest request, @PathVariable("tagId") String tagId) {
        return tag(request, tagId, 1);
    }

    /**
     * @param request
     * @param tagId
     * @param pageNum
     */
    @ApiOperation("根据标签id分页查询博文")
    @GetMapping({"/tag/{tagId}/{pageNum}"})
    public String tag(HttpServletRequest request, @PathVariable("tagId") String tagId, @PathVariable("pageNum") Integer pageNum) {
        List<BlogTagRelation> list = blogTagRelationService.list(new QueryWrapper<BlogTagRelation>()
                .lambda().eq(BlogTagRelation::getTagId, tagId));
        PageResult blogPageResult = null;
        if (!list.isEmpty()) {
            Page<BlogInfo> page = new Page<>(pageNum, 8);
            blogInfoService.page(page, new QueryWrapper<BlogInfo>()
                    .lambda()
                    .eq(BlogInfo::getBlogStatus, BlogStatusConstants.ONE)
                    .eq(BlogInfo::getIsDeleted, BlogStatusConstants.ZERO)
                    .in(BlogInfo::getBlogId, list.stream().map(BlogTagRelation::getBlogId).toArray())
                    .orderByDesc(BlogInfo::getCreateTime));
            blogPageResult = new PageResult
                    (page.getRecords(), page.getTotal(), 8, pageNum);
        }
        request.setAttribute("blogPageResult", blogPageResult);
        request.setAttribute("pageName", "标签");
        request.setAttribute("pageUrl", "tag");
        request.setAttribute("keyword", tagId);
        request.setAttribute("newBlogs", blogInfoService.getNewBlog());
        request.setAttribute("hotBlogs", blogInfoService.getHotBlog());
        request.setAttribute("hotTags", blogTagService.getBlogTagCountForIndex());
        request.setAttribute("configurations", blogConfigService.getAllConfigs());
        return "blog/" + theme + "/list";
    }
}
