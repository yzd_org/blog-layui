package com.site.blog.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.constants.SysConfigConstants;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogInfo;
import com.site.blog.entity.BlogTag;
import com.site.blog.entity.BlogTagRelation;
import com.site.blog.service.BlogInfoService;
import com.site.blog.service.BlogTagRelationService;
import com.site.blog.service.BlogTagService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
@Api(tags = "标签管理")
public class TagController {

    @Autowired
    private BlogTagService blogTagService;

    @Autowired
    private BlogInfoService blogInfoService;

    @Autowired
    private BlogTagRelationService blogTagRelationService;

    @ApiOperation("标签页")
    @GetMapping("/v1/tags")
    public String gotoTag() {
        return "/adminLayui/tag-list";
    }

    @ApiOperation("返回未删除状态下的所有标签-用于博文编辑")
    @ResponseBody
    @GetMapping("/v1/tags/list")
    public Result tagsList() {
        QueryWrapper<BlogTag> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(BlogTag::getIsDeleted, BlogStatusConstants.ZERO);
        List<BlogTag> list = blogTagService.list(queryWrapper);
        if (CollectionUtils.isEmpty(list)) {
            ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.okData(list);
    }

    @ApiOperation("标签分页列表-全部")
    @ResponseBody
    @GetMapping("/v1/tags/paging")
    public AjaxResultPage<BlogTag> getCategoryList(AjaxPutPage<BlogTag> ajaxPutPage, BlogTag condition) {
        QueryWrapper<BlogTag> queryWrapper = new QueryWrapper<>(condition);
        queryWrapper.lambda()
                .ne(BlogTag::getTagId, 1);
        Page<BlogTag> page = ajaxPutPage.putPageToPage();
        blogTagService.page(page, queryWrapper);
        AjaxResultPage<BlogTag> result = new AjaxResultPage<>();
        result.setData(page.getRecords());
        result.setCount(page.getTotal());
        return result;
    }

    @ApiOperation("删除标签")
    @ResponseBody
    @PostMapping("/v1/tags/isDel")
    @Transactional(rollbackFor = Exception.class)
    public Result updateCategoryStatus(BlogTag blogTag) {
        boolean flag = blogTagService.updateById(blogTag);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("添加标签")
    @ResponseBody
    @PostMapping("/v1/tags/add")
    @Transactional(rollbackFor = Exception.class)
    public Result addTag(BlogTag blogTag) {
        List<BlogTag> blogTagList = blogTagService.list(new QueryWrapper<BlogTag>().lambda().eq(BlogTag::getTagName, blogTag.getTagName()));
        if (!CollectionUtils.isEmpty(blogTagList)) {
            return ResponseEntity.failMessage(String.format("标签名：%s 已经存在", blogTag.getTagName()));
        }

        blogTag.setCreateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogTagService.save(blogTag);
        if (flag) {
            return ResponseEntity.ok();
        } else {
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("更新标签")
    @ResponseBody
    @PostMapping("/v1/tags/update")
    @Transactional(rollbackFor = Exception.class)
    public Result update(BlogTag blogTag) {
        List<BlogTag> blogTagList = blogTagService.list(new QueryWrapper<BlogTag>().lambda().eq(BlogTag::getTagName, blogTag.getTagName()));
        if (!CollectionUtils.isEmpty(blogTagList)) {
            return ResponseEntity.failMessage(String.format("标签名：%s 已经存在", blogTag.getTagName()));
        }
        blogTag.setCreateTime(DateUtils.getLocalCurrentDate());

        boolean flag = blogTagService.updateById(blogTag);
        if (flag) {
            return ResponseEntity.ok();
        } else {
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("清除标签")
    @ResponseBody
    @PostMapping("/v1/tags/clear")
    @Transactional(rollbackFor = Exception.class)
    public Result clearTag(Integer tagId) throws RuntimeException {
        QueryWrapper<BlogTagRelation> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(BlogTagRelation::getTagId, tagId);
        List<BlogTagRelation> tagRelationList = blogTagRelationService.list(queryWrapper);

        if (!CollectionUtils.isEmpty(tagRelationList)) {
            // blogIds
            List<BlogInfo> infoList = tagRelationList.stream()
                    .map(tagRelation -> new BlogInfo()
                            .setBlogId(tagRelation.getBlogId())
                            .setBlogTags(SysConfigConstants.DEFAULT_TAG.getConfigName())).collect(Collectors.toList());
            List<Long> blogIds = infoList.stream().map(BlogInfo::getBlogId).collect(Collectors.toList());

            // 批量更新的tagRelation信息
            List<BlogTagRelation> tagRelations = tagRelationList.stream()
                    .map(tagRelation -> new BlogTagRelation()
                            .setBlogId(tagRelation.getBlogId())
                            .setTagId(Integer.valueOf(SysConfigConstants.DEFAULT_TAG.getConfigField())))
                    .collect(Collectors.toList());
            blogInfoService.updateBatchById(infoList);


            blogTagRelationService.remove(new QueryWrapper<BlogTagRelation>()
                    .lambda()
                    .in(BlogTagRelation::getBlogId, blogIds));
            blogTagRelationService.saveBatch(tagRelations);
        }

        blogTagService.removeById(tagId);
        return ResponseEntity.ok();
    }
}
