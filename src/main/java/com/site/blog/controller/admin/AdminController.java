package com.site.blog.controller.admin;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.*;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.*;
import com.site.blog.service.*;
import com.site.blog.util.IPUtil;
import com.site.blog.util.MD5Utils;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @author 阳沐之
 * @Description: 管理员controller
 * @date: 2020年3月5日10:56:27
 */
@Controller
@RequestMapping("/admin")
@Api(tags = "用户管理")
public class AdminController {

    @Autowired
    private AdminUserService adminUserService;
    @Autowired
    private BlogInfoService blogInfoService;
    @Autowired
    private BlogTagService blogTagService;
    @Autowired
    private BlogCategoryService blogCategoryService;
    @Autowired
    private BlogCommentService blogCommentService;
    @Autowired
    private BlogConfigService blogConfigService;
    @Autowired
    private BlogLinkService blogLinkService;
    @Resource
    MailService mailService;
    @Value("${spring.mail.username}")
    private String mailTo;
    @Value("${send.mail}")
    private boolean sendMail;
    @Autowired
    private BlogLogService blogLogService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ApiOperation("登录页")
    @GetMapping(value = "/v1/login")
    public String login() {
        return "/adminLayui/login";
    }

    @ApiOperation("欢迎页")
    @GetMapping("/v1/welcome")
    public String welcome() {
        return "/adminLayui/welcome";
    }

    @ApiOperation("退出页")
    @GetMapping("/v1/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "/adminLayui/login";
    }

    @ApiOperation("编辑页")
    @GetMapping("/v1/userInfo")
    public String gotoUserInfo() {
        return "/adminLayui/userInfo-edit";
    }

    @ApiOperation("添加页")
    @GetMapping("/v1/addUser")
    public String addUserInfo() {
        return "/adminLayui/userInfo-add";
    }

    @ApiOperation("用户列表页")
    @GetMapping("/v1/users")
    public String gotoComment() {
        return "/adminLayui/users-list";
    }

    @ApiOperation("登录")
    @ResponseBody
    @PostMapping(value = "/v1/login")
    @Transactional(rollbackFor = Exception.class)
    public Result login(@RequestParam String username, @RequestParam String password,
                        HttpSession session) throws MessagingException {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            return ResponseEntity.byStatus(HttpStatusConstants.BAD_REQUEST);
        }
        QueryWrapper<AdminUser> queryWrapper = new QueryWrapper<>(
                new AdminUser().setLoginUserName(username)
                        .setLoginPassword(MD5Utils.md5EncodePassword(password))
        );
        AdminUser adminUser = adminUserService.getOne(queryWrapper);
        if (adminUser != null) {
            if (adminUser.getLocked().equals(BlogStatusConstants.ONE)) {
                //禁用
                return ResponseEntity.byStatus(HttpStatusConstants.DISABLEUSER);
            }
            session.setAttribute(SessionConstants.LOGIN_USER, adminUser.getNickName());
            session.setAttribute(SessionConstants.LOGIN_USER_ID, adminUser.getAdminUserId());
            session.setAttribute(SessionConstants.LOGIN_USER_NAME, adminUser.getLoginUserName());
            session.setAttribute(SessionConstants.AUTHOR_IMG, blogConfigService.getById(
                    SysConfigConstants.SYS_AUTHOR_IMG.getConfigField()));

            //登录记录
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            String remoteAddr = IPUtil.getIpAddr(request);
            int userId = adminUser.getAdminUserId();
            logger.info("URL : " + request.getRequestURL().toString());
            logger.info("IP : " + remoteAddr);
            logger.info("admin userId: " + userId + " , date:" + new Date());
            BlogLog tbBlogLog = new BlogLog().setUserId(userId).setDate(new Date()).setCount(1).setDescription(LogConstants.login.getDescription()).setIp(remoteAddr);
            blogLogService.save(tbBlogLog);

            if (sendMail) {
                //  邮件通知
                StringBuffer loginResult = new StringBuffer();
                loginResult.append("IP： ").append(remoteAddr).append("\n");
                loginResult.append("时间: ").append(DateUtil.formatDateTime(new Date())).append("\n");
                loginResult.append("登录ID:").append(adminUser.getAdminUserId()).append("\t登录名：")
                        .append(username).append("\t登录人：").append(adminUser.getLoginUserName()).append("\n");
                mailService.sendSimpleMail(mailTo, "请确认是否本人登录，若非本人登录，请及时修改密码！", loginResult.toString());
            }

            return ResponseEntity.okData("/blog/admin/v1/index");
        } else {
            return ResponseEntity.byStatus(HttpStatusConstants.UNAUTHORIZED);
        }
    }

    @ApiOperation("修改密码， 验证密码是否正确")
    @ResponseBody
    @GetMapping("/v1/password")
    public Result validatePassword(String oldPwd, HttpSession session) {
        Integer userId = (Integer) session.getAttribute(SessionConstants.LOGIN_USER_ID);
        boolean flag = adminUserService.validatePassword(userId, oldPwd);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.BAD_REQUEST);
    }

    @ApiOperation("返回首页相关数据")
    @GetMapping("/v1/index")
    public String index(HttpSession session) {
        session.setAttribute("categoryCount", blogCategoryService.count(
                new QueryWrapper<BlogCategory>().lambda().eq(BlogCategory::getIsDeleted,
                        BlogStatusConstants.ZERO)
        ));
        session.setAttribute("blogCount", blogInfoService.count(
                new QueryWrapper<BlogInfo>().lambda().eq(BlogInfo::getIsDeleted,
                        BlogStatusConstants.ZERO)
        ));
        session.setAttribute("linkCount", blogLinkService.count(
                new QueryWrapper<BlogLink>().lambda().eq(BlogLink::getIsDeleted,
                        BlogStatusConstants.ZERO)
        ));
        session.setAttribute("tagCount", blogTagService.count(
                new QueryWrapper<BlogTag>().lambda().eq(BlogTag::getIsDeleted,
                        BlogStatusConstants.ZERO)
        ));
        session.setAttribute("commentCount", blogCommentService.count(
                new QueryWrapper<BlogComment>().lambda().eq(BlogComment::getIsDeleted,
                        BlogStatusConstants.ZERO)
        ));
        session.setAttribute("sysList", blogConfigService.list());
        return "/adminLayui/index";
    }

    /**
     * @param session
     * @param userName
     * @param newPwd
     * @param nickName
     * @param sysAuthorImg
     * @return
     */
    @ResponseBody
    @ApiOperation("修改用户信息,成功之后清空session并跳转登录页")
    @PostMapping("/v1/userInfo")
    @Transactional(rollbackFor = Exception.class)
    public Result userInfoUpdate(HttpSession session, String userName, String newPwd,
                                 String nickName, String sysAuthorImg) throws MessagingException {
        if (StringUtils.isEmpty(newPwd) || StringUtils.isEmpty(nickName)) {
            return ResponseEntity.byStatus(HttpStatusConstants.BAD_REQUEST);
        }
        Integer loginUserId = (int) session.getAttribute(SessionConstants.LOGIN_USER_ID);
        BlogConfig blogConfig = new BlogConfig()
                .setConfigField(SysConfigConstants.SYS_AUTHOR_IMG.getConfigField())
                .setConfigValue(sysAuthorImg);
        AdminUser adminUser = new AdminUser()
                .setAdminUserId(loginUserId)
                .setLoginUserName(userName)
                .setNickName(nickName)
                .setLoginPassword(MD5Utils.md5EncodePassword(newPwd));
        if (adminUserService.updateUserInfo(adminUser, blogConfig)) {

            if (sendMail) {
                //  邮件通知
                StringBuffer updateResult = new StringBuffer();
                updateResult.append("时间: ").append(DateUtil.formatDateTime(new Date())).append("\n");
                updateResult.append("ID:").append(adminUser.getAdminUserId()).append("\t登录名：")
                        .append(adminUser.getLoginUserName()).append("\t新密码：").append(newPwd).append("\n");
                mailService.sendSimpleMail(mailTo, adminUser.getLoginUserName() + ",您的密码已修改", updateResult.toString());
            }

            //修改成功后清空session中的数据，前端控制跳转至登录页
            return ResponseEntity.okData("/blog/admin/v1/logout");
        } else {
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param ajaxPutPage
     * @param condition
     * @return
     */
    @ResponseBody
    @ApiOperation("用户列表")
    @GetMapping("/v1/userInfos")
    public AjaxResultPage<AdminUser> getUserList(AjaxPutPage<AdminUser> ajaxPutPage, AdminUser condition) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Integer userId = (Integer) request.getSession().getAttribute(SessionConstants.LOGIN_USER_ID);

        QueryWrapper<AdminUser> queryWrapper = new QueryWrapper<>(condition);
        queryWrapper.lambda().orderByAsc(AdminUser::getAdminUserId).ne(AdminUser::getAdminUserId, userId);
        Page<AdminUser> page = ajaxPutPage.putPageToPage();
        adminUserService.page(page, queryWrapper);
        AjaxResultPage<AdminUser> result = new AjaxResultPage<>();
        result.setData(page.getRecords());
        result.setCount(page.getTotal());
        result.setCode(HttpStatus.OK.value());
        return result;
    }

    /**
     * @param adminUser
     * @return
     */
    @ResponseBody
    @ApiOperation("禁用用户")
    @PostMapping(value = {"/v1/user/locked"})
    @Transactional(rollbackFor = Exception.class)
    public Result updateUserStatus(AdminUser adminUser) {
        boolean flag = adminUserService.updateById(adminUser);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ResponseBody
    @ApiOperation("清除用户")
    @PostMapping(value = {"/v1/user/clear"})
    @Transactional(rollbackFor = Exception.class)
    public Result deleteUser(AdminUser adminUser) {
        boolean flag = adminUserService.removeById(adminUser);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ResponseBody
    @ApiOperation("刷新")
    @GetMapping("/v1/reload")
    public boolean reload(HttpSession session) {
        Integer userId = (Integer) session.getAttribute(SessionConstants.LOGIN_USER_ID);
        return userId != null && userId != 0;
    }

    /**
     * @param adminUser
     * @return
     */
    @ApiOperation("添加人员")
    @ResponseBody
    @PostMapping("/v1/add")
    public Result add(@RequestBody AdminUser adminUser) {
        String loginUserName = adminUser.getLoginUserName();
        List<AdminUser> existAdminUsers = adminUserService.list(new QueryWrapper<AdminUser>().lambda().eq(AdminUser::getLoginUserName, loginUserName));
        if (!CollectionUtils.isEmpty(existAdminUsers)) {
            return ResponseEntity.failMessage(String.format("人员 %s 已存在", loginUserName));
        }
        adminUser.setCreateTime(new Date()).setUpdateTime(new Date()).setLocked(BlogStatusConstants.ZERO);
        adminUser.setLoginPassword(DigestUtils.md5DigestAsHex(adminUser.getLoginPassword().getBytes()));
        adminUserService.save(adminUser);
        return ResponseEntity.ok();
    }

}
