package com.site.blog.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.constants.FileConstants;
import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.constants.UploadConstants;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogFile;
import com.site.blog.entity.BlogInfo;
import com.site.blog.entity.BlogTagRelation;
import com.site.blog.service.BlogFileService;
import com.site.blog.service.BlogInfoService;
import com.site.blog.service.BlogTagRelationService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.MyBlogUtils;
import com.site.blog.util.ResponseEntity;
import com.site.blog.util.UploadFileUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author yzd
 */
@Controller
@RequestMapping("/admin")
@Api(tags = "博文管理")
public class BlogController {

    @Autowired
    private BlogInfoService blogInfoService;
    @Autowired
    private BlogTagRelationService blogTagRelationService;

    @Autowired
    private BlogFileService blogFileService;

    @ApiOperation("跳转博客编辑界面")
    @GetMapping("/v1/blog/edit")
    public String gotoBlogEdit(@RequestParam(required = false) Long blogId, Model model) {
        if (blogId != null) {
            BlogInfo blogInfo = blogInfoService.getById(blogId);
            List<BlogTagRelation> blogTagRelationList = blogTagRelationService.list(
                    new QueryWrapper<BlogTagRelation>()
                            .lambda()
                            .eq(BlogTagRelation::getBlogId, blogId)
            );
            List<Integer> tags = null;
            if (!CollectionUtils.isEmpty(blogTagRelationList)) {
                tags = blogTagRelationList.stream().map(
                        BlogTagRelation::getTagId)
                        .collect(Collectors.toList());
            }
            model.addAttribute("blogTags", tags);
            model.addAttribute("blogInfo", blogInfo);
        }
        return "/adminLayui/blog-edit";
    }

    @ApiOperation("跳转博客列表界面")
    @GetMapping("/v1/blog")
    public String gotoBlogList() {
        return "/adminLayui/blog-list";
    }

    @ApiOperation("保存文章图片")
    @ResponseBody
    @PostMapping("/v1/blog/uploadFile")
    public Map<String, Object> uploadFileByEditormd(HttpServletRequest request,
                                                    @RequestParam(name = "editormd-image-file")
                                                            MultipartFile file) throws URISyntaxException {
        String suffixName = UploadFileUtils.getSuffixName(file);
        //生成文件名称通用方法
        String newFileName = UploadFileUtils.getNewFileName(suffixName);
        File fileDirectory = new File(UploadConstants.FILE_UPLOAD_DIC);
        //创建文件
        File destFile = new File(UploadConstants.FILE_UPLOAD_DIC + newFileName);
        Map<String, Object> result = new HashMap<>();
        try {
            if (!fileDirectory.exists()) {
                if (!fileDirectory.mkdirs()) {
                    throw new IOException("文件夹创建失败,路径为：" + fileDirectory);
                }
            }
            file.transferTo(destFile);
            String fileUrl = MyBlogUtils.getHost(new URI(request.getRequestURL() + "")) + "/blog" +
                    UploadConstants.FILE_SQL_DIC + newFileName;

            BlogFile blogFile = new BlogFile();
            blogFile.setName(newFileName);
            blogFile.setType(FileConstants.image.getType());
            blogFile.setCreateTime(new Date());
            blogFile.setUrl(fileUrl);
            blogFile.setUpdateTime(new Date());
            blogFileService.insert(blogFile);

            result.put("success", 1);
            result.put("message", "上传成功");
            result.put("url", fileUrl);
        } catch (IOException e) {
            result.put("success", 0);
        }
        return result;
    }

    @ApiOperation("保存文章内容")
    @ResponseBody
    @PostMapping("/v1/blog/edit")
    public Result saveBlog(@RequestParam("blogTagIds[]") List<Integer> blogTagIds, BlogInfo blogInfo) {
        if (CollectionUtils.isEmpty(blogTagIds) || ObjectUtils.isEmpty(blogInfo)) {
            return ResponseEntity.byStatus(HttpStatusConstants.BAD_REQUEST);
        }
        blogInfo.setUpdateTime(DateUtils.getLocalCurrentDate());
        if (blogInfoService.saveOrUpdate(blogInfo)) {
            //远处原来关系 批量保存现在
            blogTagRelationService.removeAndSaveBatch(blogTagIds, blogInfo);
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    /**
     * @param ajaxPutPage 分页参数
     * @param condition   筛选条件
     * @date 2020年4月28日17:56:47
     */
    @ApiOperation("文章分页列表")
    @ResponseBody
    @GetMapping("/v1/blog/list")
    public AjaxResultPage<BlogInfo> getContractList(AjaxPutPage<BlogInfo> ajaxPutPage, BlogInfo condition) {
        QueryWrapper<BlogInfo> queryWrapper = new QueryWrapper<>(condition);
        queryWrapper.lambda().orderByDesc(BlogInfo::getUpdateTime);
        Page<BlogInfo> page = ajaxPutPage.putPageToPage();
        blogInfoService.page(page, queryWrapper);
        AjaxResultPage<BlogInfo> result = new AjaxResultPage<>();
        result.setData(page.getRecords());
        result.setCount(page.getTotal());
        return result;
    }

    @ApiOperation("修改博客的部分状态相关信息")
    @ResponseBody
    @PostMapping("/v1/blog/blogStatus")
    @Transactional(rollbackFor = Exception.class)
    public Result updateBlogStatus(BlogInfo blogInfo) {
        blogInfo.setUpdateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogInfoService.updateById(blogInfo);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("修改文章的删除状态为已删除")
    @ResponseBody
    @PostMapping("/v1/blog/delete")
    @Transactional(rollbackFor = Exception.class)
    public Result deleteBlog(@RequestParam Long blogId) {
        BlogInfo blogInfo = new BlogInfo()
                .setBlogId(blogId)
                .setIsDeleted(BlogStatusConstants.ONE)
                .setUpdateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogInfoService.updateById(blogInfo);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("清除文章")
    @ResponseBody
    @PostMapping("/v1/blog/clear")
    @Transactional(rollbackFor = Exception.class)
    public Result clearBlog(@RequestParam Long blogId) {
        if (blogInfoService.clearBlogInfo(blogId)) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("更改文章成未删除状态")
    @ResponseBody
    @PostMapping("/v1/blog/restore")
    @Transactional(rollbackFor = Exception.class)
    public Result restoreBlog(@RequestParam Long blogId) {
        BlogInfo blogInfo = new BlogInfo()
                .setBlogId(blogId)
                .setIsDeleted(BlogStatusConstants.ZERO)
                .setUpdateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogInfoService.updateById(blogInfo);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("所有博客")
    @ResponseBody
    @GetMapping("v1/blog/select")
    public List<BlogInfo> getBlogInfoSelect() {
        return blogInfoService.list();
    }

}
