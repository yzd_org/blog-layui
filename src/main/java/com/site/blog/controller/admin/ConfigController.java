package com.site.blog.controller.admin;

import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogConfig;
import com.site.blog.service.BlogConfigService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author yzd
 * @classname: ConfigController
 * @description: blog配置controller
 **/
@Controller
@RequestMapping("/admin")
@Api(tags = "系统配置管理")
public class ConfigController {

    @Autowired
    private BlogConfigService blogConfigService;

    @ApiOperation("系统配置界面页")
    @GetMapping("/v1/blogConfig")
    public String gotoBlogConfig(){
        return "/adminLayui/sys-edit";
    }

    @ApiOperation("系统配置信息列表")
    @ResponseBody
    @GetMapping("/v1/blogConfig/list")
    public AjaxResultPage<BlogConfig> getBlogConfig(){
        AjaxResultPage<BlogConfig> ajaxResultPage = new AjaxResultPage<>();
        List<BlogConfig> list = blogConfigService.list();
        if (CollectionUtils.isEmpty(list)){
            ajaxResultPage.setCode(500);
            return ajaxResultPage;
        }
        ajaxResultPage.setData(blogConfigService.list());
        return ajaxResultPage;
    }

    @ApiOperation("修改系统信息")
    @ResponseBody
    @PostMapping("/v1/blogConfig/edit")
    @Transactional(rollbackFor = Exception.class)
    public Result updateBlogConfig(BlogConfig blogConfig){
        blogConfig.setUpdateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogConfigService.updateById(blogConfig);
        if (flag){
            return ResponseEntity.ok();
        }else{
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("新增系统信息页")
    @GetMapping("/v1/blogConfig/add")
    public String addBlogConfig(){
        return "adminLayui/sys-add";
    }

    @ApiOperation("新增系统信息项")
    @ResponseBody
    @PostMapping("/v1/blogConfig/add")
    @Transactional(rollbackFor = Exception.class)
    public Result addBlogConfig(BlogConfig blogConfig){
        blogConfig.setCreateTime(DateUtils.getLocalCurrentDate());
        blogConfig.setUpdateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogConfigService.save(blogConfig);
        if (flag){
            return ResponseEntity.ok();
        }else{
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("删除配置信息项")
    @ResponseBody
    @PostMapping("/v1/blogConfig/del")
    @Transactional(rollbackFor = Exception.class)
    public Result delBlogConfig(@RequestParam String configField){
        boolean flag = blogConfigService.removeById(configField);
        if (flag){
            return ResponseEntity.ok();
        }else{
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }
}
