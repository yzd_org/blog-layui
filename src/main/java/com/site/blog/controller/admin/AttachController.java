package com.site.blog.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.FileConstants;
import com.site.blog.constants.LogConstants;
import com.site.blog.constants.SessionConstants;
import com.site.blog.constants.UploadConstants;
import com.site.blog.controller.vo.BlogFileVo;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogFile;
import com.site.blog.entity.BlogLog;
import com.site.blog.service.BlogFileService;
import com.site.blog.service.BlogLogService;
import com.site.blog.util.MyBlogUtils;
import com.site.blog.util.ResponseEntity;
import com.site.blog.util.UploadFileUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 附件管理
 *
 * @author yzd
 */
@Controller
@RequestMapping("admin/attach")
@Api(tags = "附件管理")
public class AttachController {
    private static final Logger logger = LoggerFactory.getLogger(AttachController.class);

    @Autowired
    private BlogFileService blogFileService;
    @Autowired
    private BlogLogService blogLogService;

    @ApiOperation("文件列表页")
    @GetMapping(value = "/listPage")
    public String listPage() {
        return "/adminLayui/file-list";
    }

    @ApiOperation("文件列表")
    @GetMapping(value = "/listAll")
    @ResponseBody
    public AjaxResultPage<BlogFileVo> listAll(AjaxPutPage<BlogFileVo> ajaxPutPage, BlogFile blogFile) {
        IPage<BlogFile> page = blogFileService.page(new Page<>(ajaxPutPage.getPage(), ajaxPutPage.getLimit())
                , new QueryWrapper<>(blogFile).lambda().orderByDesc(BlogFile::getCreateTime));
        AjaxResultPage<BlogFileVo> result = new AjaxResultPage<>();
        List<BlogFile> records = page.getRecords();
        List<BlogFileVo> data = new ArrayList<>();
            for (BlogFile record : records) {
                BlogFileVo blogFileVo = new BlogFileVo();
                for (FileConstants fileConstants : FileConstants.values()) {
                    if (record.getType() == fileConstants.getType()) {
                        BeanUtils.copyProperties(record, blogFileVo);
                        blogFileVo.setType(fileConstants.getDescription());
                    }
                }
                data.add(blogFileVo);
            }
        result.setData(data);
        result.setCount(page.getTotal());
        result.setCode(HttpStatus.OK.value());
        return result;
    }

    @ApiOperation("上传文件")
    @PostMapping("/upload")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Result upload(HttpServletRequest request, @RequestParam(required = false) int type, @RequestParam("file") MultipartFile[] files) {
        if (files.length == 0) {
            return ResponseEntity.failMessage("上传文件不能为空");
        }
        Integer userId = (Integer) request.getSession().getAttribute(SessionConstants.LOGIN_USER_ID);
        List<BlogFile> blogFiles = new ArrayList<>();
        List<BlogLog> blogLogs = new ArrayList<>();
        try {
            for (MultipartFile file : files) {
                BlogFile blogFile = new BlogFile();
                BlogLog blogLog = new BlogLog();
                String newFileName = UploadFileUtils.getFileName(file);
                // 文件存储实际路径
                File fileDirectory = new File(UploadConstants.UPLOAD);
                String pathName = fileDirectory + "/" + newFileName;
                File destFile = new File(pathName);

                if (!fileDirectory.exists()) {
                    if (!fileDirectory.mkdirs()) {
                        logger.info("文件夹创建失败,路径为：" + fileDirectory);
                        throw new IOException("文件夹创建失败,路径为：" + fileDirectory);
                    }
                }
                file.transferTo(destFile);
                String url = MyBlogUtils.getHost(new URI(request.getRequestURL() + "")) + "/blog" + UploadConstants.MYFILE + newFileName;
                blogFile.setCreateTime(new Date()).setUpdateTime(new Date()).setName(file.getOriginalFilename()).setType(type).setUrl(url);
                blogLog.setDate(new Date()).setUserId(userId).setDescription(LogConstants.uoload.getDescription());
                blogFiles.add(blogFile);
                blogLogs.add(blogLog);
            }
            blogFileService.saveBatch(blogFiles);
            blogLogService.saveBatch(blogLogs);
        } catch (Exception e) {
            return ResponseEntity.failMessage("文件上传失败");
        }
        return ResponseEntity.ok();
    }

    @ApiOperation("删除文件")
    @RequestMapping("/delete")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Result delete(@RequestParam Integer fileId, HttpServletRequest httpServletRequest) {
        try {
            BlogFile blogFile = blogFileService.queryById(fileId);
            if (null == blogFile) {
                return ResponseEntity.failMessage("不存在该文件");
            }
            if (blogFile.getType() == FileConstants.qcr.getType()) {
                return ResponseEntity.failMessage("系统二维码不能删除！");
            }
            blogFileService.deleteById(fileId);
            new File(UploadConstants.UPLOAD + blogFile.getName()).delete();
            Integer userId = (Integer) httpServletRequest.getSession().getAttribute(SessionConstants.LOGIN_USER_ID);
            BlogLog blogLog = new BlogLog().setDate(new Date()).setDescription(LogConstants.delete.getDescription()).setUserId(userId);
            blogLogService.saveOrUpdate(blogLog);
        } catch (Exception e) {
            logger.error("删除失败");
            return ResponseEntity.failMessage("删除失败");
        }
        return ResponseEntity.ok();
    }
}
