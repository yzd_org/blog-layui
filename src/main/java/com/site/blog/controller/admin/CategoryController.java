package com.site.blog.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.BlogStatusConstants;
import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.constants.SysConfigConstants;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogCategory;
import com.site.blog.entity.BlogInfo;
import com.site.blog.service.BlogCategoryService;
import com.site.blog.service.BlogInfoService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 阳沐之
 * @Description: 分类Controller
 * @date: 2020年3月5日10:59:24
 */
@Controller
@RequestMapping("/admin")
@Api(tags = "分类管理")
public class CategoryController {

    @Autowired
    private BlogCategoryService blogCategoryService;
    @Autowired
    private BlogInfoService blogInfoService;

    @ApiOperation("分类的集合数据 用于下拉框")
    @ResponseBody
    @GetMapping("/v1/category/list")
    public Result categoryList() {
        QueryWrapper<BlogCategory> queryWrapper = new QueryWrapper<BlogCategory>();
        queryWrapper.lambda().eq(BlogCategory::getIsDeleted, BlogStatusConstants.ZERO);
        List<BlogCategory> list = blogCategoryService.list(queryWrapper);
        if (CollectionUtils.isEmpty(list)) {
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.okData(list);
    }

    @ApiOperation("分类页")
    @GetMapping("/v1/category")
    public String gotoBlogCategory() {
        return "/adminLayui/category-list";
    }

    @ApiOperation("分类列表")
    @ResponseBody
    @GetMapping("/v1/category/paging")
    public AjaxResultPage<BlogCategory> getCategoryList(AjaxPutPage<BlogCategory> ajaxPutPage, BlogCategory condition) {
        QueryWrapper<BlogCategory> queryWrapper = new QueryWrapper<>(condition);
        queryWrapper.lambda()
                .orderByAsc(BlogCategory::getCategoryRank)
                .ne(BlogCategory::getCategoryId, 1);
        Page<BlogCategory> page = ajaxPutPage.putPageToPage();
        blogCategoryService.page(page, queryWrapper);
        AjaxResultPage<BlogCategory> result = new AjaxResultPage<>();
        result.setData(page.getRecords());
        result.setCount(page.getTotal());
        return result;
    }

    @ApiOperation("修改分类信息")
    @ResponseBody
    @PostMapping("/v1/category/update")
    @Transactional(rollbackFor = Exception.class)
    public Result updateCategory(BlogCategory blogCategory) {
        //        检查分类名是否存在
        String categoryName = blogCategory.getCategoryName();
        List<BlogCategory> blogCategoryList = blogCategoryService.list(new QueryWrapper<BlogCategory>().lambda()
                .eq(BlogCategory::getCategoryName, categoryName));
        if (!CollectionUtils.isEmpty(blogCategoryList)) {
            return ResponseEntity.failMessage(String.format("分类名：%s 已经存在", categoryName));
        }

        BlogInfo blogInfo = new BlogInfo()
                .setBlogCategoryId(blogCategory.getCategoryId())
                .setBlogCategoryName(blogCategory.getCategoryName());
        UpdateWrapper<BlogInfo> updateWrapper = new UpdateWrapper<>();

        updateWrapper.lambda().eq(BlogInfo::getBlogCategoryId, blogCategory.getCategoryId());
        blogInfoService.update(blogInfo, updateWrapper);
        boolean flag = blogCategoryService.updateById(blogCategory);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("修改分类状态")
    @ResponseBody
    @PostMapping("/v1/category/isDel")
    public Result updateCategoryStatus(BlogCategory blogCategory) {
        boolean flag = blogCategoryService.updateById(blogCategory);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("清除分类")
    @ResponseBody
    @PostMapping("/v1/category/clear")
    @Transactional(rollbackFor = Exception.class)
    public Result clearCategory(BlogCategory blogCategory) {
        Integer categoryId = blogCategory.getCategoryId();
        QueryWrapper<BlogInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(BlogInfo::getBlogCategoryId, categoryId);
        List<BlogInfo> blogInfos = blogInfoService.list(queryWrapper);

        if (!CollectionUtils.isEmpty(blogInfos)) {
            // 批量更新的信息
            List<BlogInfo> infoList = blogInfos.stream()
                    .map(item -> new BlogInfo()
                            .setBlogId(item.getBlogId())
                            .setBlogCategoryId(Integer.valueOf(SysConfigConstants.DEFAULT_CATEGORY.getConfigField()))
                            .setBlogCategoryName(SysConfigConstants.DEFAULT_CATEGORY.getConfigName()))
                    .collect(Collectors.toList());
            blogInfoService.updateBatchById(infoList);
        }

        boolean flag = blogCategoryService.removeById(categoryId);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("添加分类页")
    @GetMapping("/v1/category/add")
    public String addBlogConfig() {
        return "/adminLayui/category-add";
    }

    @ApiOperation("添加分类")
    @ResponseBody
    @PostMapping("/v1/category/add")
    @Transactional(rollbackFor = Exception.class)
    public Result addCategory(BlogCategory blogCategory) {
//        检查分类名是否存在
        String categoryName = blogCategory.getCategoryName();
        List<BlogCategory> blogCategoryList = blogCategoryService.list(new QueryWrapper<BlogCategory>().lambda()
                .eq(BlogCategory::getCategoryName, categoryName));
        if (!CollectionUtils.isEmpty(blogCategoryList)) {
            return ResponseEntity.failMessage(String.format("分类名：%s 已经存在", categoryName));
        }

        blogCategory.setCreateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogCategoryService.save(blogCategory);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

}
