package com.site.blog.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogComment;
import com.site.blog.service.BlogCommentService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author yzd
 * @Description: 评论标签
 * @date: 2020年3月5日10:57:09
 */
@Controller
@RequestMapping("/admin")
@Api(tags = "评论管理")
public class CommentController {

    @Resource
    private BlogCommentService blogCommentService;

    @ApiOperation("评论页")
    @GetMapping("/v1/comment")
    public String gotoComment() {
        return "/adminLayui/comment-list";
    }

    @ApiOperation("评论列表")
    @ResponseBody
    @GetMapping("/v1/comment/paging")
    public AjaxResultPage<BlogComment> getLinkList(AjaxPutPage<BlogComment> ajaxPutPage, BlogComment condition) {
        QueryWrapper<BlogComment> queryWrapper = new QueryWrapper<>(condition);
        Page<BlogComment> page = ajaxPutPage.putPageToPage();
        blogCommentService.page(page, queryWrapper);
        AjaxResultPage<BlogComment> result = new AjaxResultPage<>();
        result.setData(page.getRecords());
        result.setCount(page.getTotal());
        result.setCode(HttpStatus.OK.value());
        return result;
    }

    @ApiOperation("更新评论状态状态：1.删除状态 2.审核状态")
    @ResponseBody
    @PostMapping(value = {"/v1/comment/isDel", "/v1/comment/commentStatus"})
    @Transactional(rollbackFor = Exception.class)
    public Result updateLinkStatus(BlogComment blogComment) {
        boolean flag = blogCommentService.updateById(blogComment);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("回复")
    @ResponseBody
    @PostMapping("/v1/comment/edit")
    @Transactional(rollbackFor = Exception.class)
    public Result updateBlogConfig(BlogComment blogComment) {
        blogComment.setReplyCreateTime(DateUtils.getLocalCurrentDate());
        boolean flag = blogCommentService.updateById(blogComment);
        if (flag) {
            return ResponseEntity.ok();
        } else {
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("清除评论")
    @ResponseBody
    @PostMapping("/v1/comment/clear")
    @Transactional(rollbackFor = Exception.class)
    public Result clearLink(Integer commentId) {
        boolean flag = blogCommentService.removeById(commentId);
        if (flag) {
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }


}
