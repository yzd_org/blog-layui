package com.site.blog.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.site.blog.controller.vo.DayVo;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogLog;
import com.site.blog.response.DayVisit;
import com.site.blog.scheduling.Scheduling;
import com.site.blog.service.BlogLogService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.ResponseEntity;
import com.site.blog.util.SystemUsageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/admin")
@Api(tags = "系统统计")
public class SysDataController {

    @Autowired
    private BlogLogService blogLogService;

    @GetMapping("/v1/sysData")
    public String gotoBlogConfig() {
        return "/adminLayui/sys-data";
    }

    @ApiOperation("12日系统统计")
    @ResponseBody
    @GetMapping("/v1/sysDataPage")
    public Result listAll() throws ParseException {
        Date pastDate = DateUtils.getPastDate(30);
        List<BlogLog> blogList = blogLogService.list(new QueryWrapper<BlogLog>().lambda().ge(BlogLog::getDate, pastDate));
        SortedMap<Date, List<DayVisit>> sort = Scheduling.dateCount(blogList);
        List<DayVo> result = new ArrayList<>();
        for (Map.Entry<Date, List<DayVisit>> stringListEntry : sort.entrySet()) {
            SimpleDateFormat format = new SimpleDateFormat("MM-dd");
            String date = format.format(stringListEntry.getKey());
            int count = stringListEntry.getValue().size();
            DayVo dayVo = new DayVo();
            dayVo.setDay(date);
            dayVo.setCount(count);
            result.add(dayVo);
        }
        return ResponseEntity.okData(result.subList(result.size() - 12, result.size()));
    }

    @ApiOperation("内存使用量")
    @ResponseBody
    @GetMapping("/v1/getMemory")
    public Result getMemory() {
        double memoryUsage = SystemUsageUtil.getMemoryUsage();
        return ResponseEntity.okData(memoryUsage * 100);
    }

}
