package com.site.blog.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.constants.LinkConstants;
import com.site.blog.controller.vo.BlogLinkVo;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogLink;
import com.site.blog.service.BlogLinkService;
import com.site.blog.util.DateUtils;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 阳沐之
 * @Description: 友链Controller
 * @date: 2020年3月5日10:58:21
 */
@Controller
@RequestMapping("/admin")
@Api(tags = "友链管理")
public class LinkController {

    @Autowired
    private BlogLinkService blogLinkService;

    @ApiOperation("友链页")
    @GetMapping("/v1/linkType")
    public String gotoLink(){
        return "/adminLayui/link-list";
    }

    @ApiOperation("获取友链类型列表-用于查询")
    @ResponseBody
    @GetMapping("/v1/linkType/list")
    public Result linkTypeList(){
        List<BlogLink> links = new ArrayList<>();
        links.add(new BlogLink().setLinkType(LinkConstants.LINK_TYPE_FRIENDSHIP.getLinkTypeId())
                .setLinkName(LinkConstants.LINK_TYPE_FRIENDSHIP.getLinkTypeName()));
        links.add(new BlogLink().setLinkType(LinkConstants.LINK_TYPE_RECOMMEND.getLinkTypeId())
                .setLinkName(LinkConstants.LINK_TYPE_RECOMMEND.getLinkTypeName()));
        links.add(new BlogLink().setLinkType(LinkConstants.LINK_TYPE_PRIVATE.getLinkTypeId())
                .setLinkName(LinkConstants.LINK_TYPE_PRIVATE.getLinkTypeName()));
        return ResponseEntity.okData(links);
    }

    @ApiOperation("获取分页友链列表")
    @ResponseBody
    @GetMapping("/v1/link/paging")
    public AjaxResultPage<BlogLinkVo> getLinkList(AjaxPutPage<BlogLink> ajaxPutPage, BlogLink condition) {
        QueryWrapper<BlogLink> queryWrapper = new QueryWrapper<>(condition);
        queryWrapper.lambda()
                .orderByAsc(BlogLink::getLinkRank);
        Page<BlogLink> page = ajaxPutPage.putPageToPage();
        blogLinkService.page(page,queryWrapper);
        AjaxResultPage<BlogLinkVo> result = new AjaxResultPage<>();

        List<BlogLink> records = page.getRecords();
        List<BlogLinkVo> blogLinkVos = new ArrayList<>();
        records.stream().forEach(item -> {
            for (LinkConstants value : LinkConstants.values()) {
                if (value.getLinkTypeId().equals(item.getLinkType())) {
                    BlogLinkVo blogLinkVo = new BlogLinkVo();
                    BeanUtils.copyProperties(item, blogLinkVo);
                    blogLinkVo.setLinkType(value.getLinkTypeName());
                    blogLinkVos.add(blogLinkVo);
                }
            }
        });
        result.setData(blogLinkVos);
        result.setCount(page.getTotal());
        return result;
    }

    @ApiOperation("更新友链删除状态")
    @ResponseBody
    @PostMapping("/v1/link/isDel")
    @Transactional(rollbackFor = Exception.class)
    public Result updateLinkStatus(BlogLink blogLink){
        boolean flag = blogLinkService.updateById(blogLink);
        if (flag){
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("根据id清除友链")
    @ResponseBody
    @PostMapping("/v1/link/clear")
    @Transactional(rollbackFor = Exception.class)
    public Result clearLink(Integer linkId){
        boolean flag = blogLinkService.removeById(linkId);
        if (flag){
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }

    @ApiOperation("根据id编辑友链-信息")
    @GetMapping("/v1/link/edit")
    public String editLink(Integer linkId, Model model){
        if (linkId != null){
            BlogLink blogLink = blogLinkService.getById(linkId);
            model.addAttribute("blogLink",blogLink);
        }
        return "/adminLayui/link-edit";
    }

    @ApiOperation("保存编辑的友链信息")
    @ResponseBody
    @PostMapping("/v1/link/edit")
    @Transactional(rollbackFor = Exception.class)
    public Result updateAndSaveLink(BlogLink blogLink){
        blogLink.setCreateTime(DateUtils.getLocalCurrentDate());
        boolean flag;
        if (blogLink.getLinkId() != null){
            flag = blogLinkService.updateById(blogLink);
        }else{
            flag = blogLinkService.save(blogLink);
        }
        if (flag){
            return ResponseEntity.ok();
        }
        return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
    }
}
