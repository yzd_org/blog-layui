package com.site.blog.controller.admin;

import com.site.blog.constants.FileConstants;
import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.constants.SysConfigConstants;
import com.site.blog.constants.UploadConstants;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogConfig;
import com.site.blog.entity.BlogFile;
import com.site.blog.service.BlogConfigService;
import com.site.blog.service.BlogFileService;
import com.site.blog.util.MyBlogUtils;
import com.site.blog.util.ResponseEntity;
import com.site.blog.util.UploadFileUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

/**
 * @author 阳沐之
 * 用户头像上传
 */
@Controller
@RequestMapping("/admin")
@Api(tags = "头像上传")
public class UploadController {
    @Autowired
    private BlogConfigService blogConfigService;

    @Autowired
    private BlogFileService blogFileService;

    private final Logger logger = LoggerFactory.getLogger(UploadController.class);

    @ApiOperation("用户头像上传")
    @PostMapping({"/upload/authorImg"})
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Result upload(HttpServletRequest httpServletRequest, @RequestParam("file") MultipartFile file) throws URISyntaxException {
        String suffixName = UploadFileUtils.getSuffixName(file);
        String newFileName = UploadFileUtils.getNewFileName(suffixName);
        File fileDirectory = new File(UploadConstants.UPLOAD_AUTHOR_IMG);
        String pathName = UploadConstants.UPLOAD_AUTHOR_IMG + newFileName;
        File destFile = new File(pathName);
        try {
            if (!fileDirectory.exists()) {
                if (!fileDirectory.mkdirs()) {
                    logger.info("文件夹创建失败,路径为：" + fileDirectory);
                    throw new IOException("文件夹创建失败,路径为：" + fileDirectory);
                }
            }
            file.transferTo(destFile);
            String url = MyBlogUtils.getHost(new URI(httpServletRequest.getRequestURL() + "")) + "/blog" + UploadConstants.SQL_AUTHOR_IMG + newFileName;
            BlogConfig blogConfig = new BlogConfig();
            blogConfig.setConfigField(SysConfigConstants.SYS_AUTHOR_IMG.getConfigField());
            blogConfig.setUpdateTime(new Date());
            blogConfig.setConfigValue(url);
            blogConfigService.updateById(blogConfig);

            BlogFile blogFile = new BlogFile();
            blogFile.setName("头像_" + newFileName).setType(FileConstants.image.getType()).setCreateTime(new Date()).setUrl(url).setUpdateTime(new Date());
            blogFileService.insert(blogFile);

            return ResponseEntity.okData(url);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.byStatus(HttpStatusConstants.INTERNAL_SERVER_ERROR);
        }
    }


}
