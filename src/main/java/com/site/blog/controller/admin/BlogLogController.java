package com.site.blog.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.site.blog.constants.LogConstants;
import com.site.blog.constants.SessionConstants;
import com.site.blog.controller.vo.AdminLog;
import com.site.blog.dto.AjaxPutPage;
import com.site.blog.dto.AjaxResultPage;
import com.site.blog.dto.Result;
import com.site.blog.entity.BlogLog;
import com.site.blog.service.BlogLogService;
import com.site.blog.util.ResponseEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yzd
 * @since 2020-03-07
 */
@Controller
@RequestMapping("/admin")
@Api(tags = "日志管理")
public class BlogLogController {

    @Autowired
    private BlogLogService blogLogService;

    @ApiOperation("操作日志列表")
    @GetMapping(value = "/listOperateAll")
    public String listAll(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "limit", defaultValue = "15") int limit, Model model) {
        IPage<BlogLog> result = blogLogService.page(new Page<>(page, limit), new QueryWrapper<BlogLog>().lambda().orderByDesc(BlogLog::getDate)
                .ne(BlogLog::getDescription, LogConstants.login.getDescription())
                .or().ne(BlogLog::getDescription, LogConstants.logout.getDescription()));
        model.addAttribute("pages", result);
        return "/admin/log";
    }

    @ApiOperation("登录日志-页面")
    @GetMapping(value = "/loginLog")
    public String login() {
        return "/adminLayui/log";
    }

    @ResponseBody
    @ApiOperation("当前登录退出日志")
    @GetMapping("/v1/history")
    public AjaxResultPage<AdminLog> history(HttpServletRequest request, AjaxPutPage<BlogLog> ajaxPutPage, BlogLog blogLog) {
        int userId = (int) request.getSession().getAttribute(SessionConstants.LOGIN_USER_ID);
        IPage<BlogLog> page = blogLogService.page(new Page<>(ajaxPutPage.getPage(), ajaxPutPage.getLimit()),
                new QueryWrapper<>(blogLog).lambda()
                        .eq(BlogLog::getUserId, userId).orderByDesc(BlogLog::getDate)
                        .eq(BlogLog::getDescription, LogConstants.login.getDescription())
                        .or().eq(BlogLog::getDescription, LogConstants.logout.getDescription()));

        List<AdminLog> adminLogs = new ArrayList<>();
        List<BlogLog> blogLogs = page.getRecords();
        if (!CollectionUtils.isEmpty(blogLogs)) {
            blogLogs.forEach(item -> {
                AdminLog adminLog = new AdminLog();
                BeanUtils.copyProperties(item, adminLog);
                adminLogs.add(adminLog);
            });
        }
        AjaxResultPage<AdminLog> result = new AjaxResultPage<>();
        result.setData(adminLogs);
        result.setCount(page.getTotal());
        result.setCode(HttpStatus.OK.value());
        return result;
    }

    @ResponseBody
    @ApiOperation("删除日志")
    @PostMapping(value = "/deleteLog")
    public Result deleteLog(@RequestParam Integer id) {
        blogLogService.removeById(id);
        return ResponseEntity.ok();
    }

    @ResponseBody
    @ApiOperation("访问控制")
    @GetMapping(value = "/operate")
    public AjaxResultPage<AdminLog> operate(AjaxPutPage<BlogLog> ajaxPutPage, BlogLog blogLog) {
        IPage<BlogLog> page = blogLogService.page(new Page<>(ajaxPutPage.getPage(), ajaxPutPage.getLimit()),
                new QueryWrapper<>(blogLog).lambda()
                        .orderByDesc(BlogLog::getDate)
                        .eq(BlogLog::getDescription, LogConstants.visit.getDescription()));
        AjaxResultPage<AdminLog> result = new AjaxResultPage<>();
        result.setData(null);
        result.setCount(page.getTotal());
        result.setCode(HttpStatus.OK.value());
        return result;
    }

    @ApiOperation("禁用Ip")
    @GetMapping(value = "/DisableIp")
    public String disableIp() {
        return "/adminLayui/log";
    }
}

