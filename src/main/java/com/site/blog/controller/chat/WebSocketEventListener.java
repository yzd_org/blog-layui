package com.site.blog.controller.chat;

import com.site.blog.entity.ChatMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

/**
 * WebSocket事件侦听
 *
 * @author yzd
 */
@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    private static Integer count = 0;
    /**
     * WebSocket消息发送对象
     */
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    /**
     * 连接事件侦听
     *
     * @param event
     */
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        WebSocketEventListener.addOnlineCount();
        logger.info("接收到一个新的连接,当前在线人数为：" + count);
    }

    /**
     * 断开事件侦听
     *
     * @param event
     */
    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());

        String username = (String) headerAccessor.getSessionAttributes().get("username");
        if (username != null) {
            WebSocketEventListener.subOnlineCount();
            logger.info(String.format("用户 : %s 离开，当前在线人数为： %s", username, count));

            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setType(ChatMessage.MessageType.LEAVE);
            chatMessage.setSender(username);
            messagingTemplate.convertAndSend("/topic/public", chatMessage);
        }
    }

    public static synchronized void addOnlineCount() {
        WebSocketEventListener.count++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketEventListener.count--;
    }
}
