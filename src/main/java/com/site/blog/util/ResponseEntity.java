package com.site.blog.util;

import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.dto.Result;
import io.swagger.annotations.ApiModel;
import org.springframework.util.StringUtils;

/**
 * 响应结果生成工具
 * @author 阳沐之
 */
@ApiModel("响应结果生成工具")
public class ResponseEntity {

    public static Result ok() {
        Result result = new Result();
        result.setCode(HttpStatusConstants.OK.getStatus());
        result.setMessage(HttpStatusConstants.OK.getContent());
        return result;
    }

    public static Result okMessage(String message) {
        Result result = new Result();
        result.setCode(HttpStatusConstants.OK.getStatus());
        result.setMessage(message);
        return result;
    }

    public static Result<Object> okData(Object data) {
        Result<Object> result = new Result<>();
        result.setCode(HttpStatusConstants.OK.getStatus());
        result.setMessage(HttpStatusConstants.OK.getContent());
        result.setData(data);
        return result;
    }

    public static Result failMessage(String message) {
        Result result = new Result();
        result.setCode(HttpStatusConstants.INTERNAL_SERVER_ERROR.getStatus());
        if (StringUtils.isEmpty(message)) {
            result.setMessage(HttpStatusConstants.INTERNAL_SERVER_ERROR.getContent());
        } else {
            result.setMessage(message);
        }
        return result;
    }

    public static Result codeAndMessage(int code, String message) {
        Result result = new Result();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    public static Result byStatusAndData(HttpStatusConstants constants, Object data) {
        Result result = ResponseEntity.codeAndMessage(constants.getStatus(), constants.getContent());
        result.setData(data);
        return result;
    }


    public static Result byStatus(HttpStatusConstants constants) {
        Result result = ResponseEntity.codeAndMessage(constants.getStatus(), constants.getContent());
        return result;
    }
}
