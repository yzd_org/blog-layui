package com.site.blog.util;

import org.springframework.util.DigestUtils;

/**
 * @Description md5加密
 * @author 阳沐之
 */
public class MD5Utils {
    public static String md5EncodePassword(String password) {
        return DigestUtils.md5DigestAsHex(password.getBytes());
    }

    public static void main(String[] args) {
        System.out.println(MD5Utils.md5EncodePassword("admin"));
    }

}
