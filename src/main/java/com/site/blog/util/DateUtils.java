package com.site.blog.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * @author yzd
 */
public class DateUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);

    /**
     * 获得本地当前时间(时间戳)
     */
    public static Timestamp getLocalCurrentDate(){
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return
     */
    public static Date getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String result = format.format(today);
        LOGGER.info(result);
        return today;
    }

    /**
     * 获取周一时间
     *
     * @return
     */
    public static Date geetMonday() {
        LocalDate localDate = LocalDate.now();
        LocalDate monday = localDate.with(DayOfWeek.MONDAY);
        ZoneId zone = ZoneId.systemDefault();
        Instant startInstant = monday.atStartOfDay().atZone(zone).toInstant();
        Date date = Date.from(startInstant);
        return date;
    }

    /**
     * 格式化时间
     *
     * @param date
     * @return
     */
    public static String dateFormat(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return date == null ? format.format(new Date()) : format.format(date);
    }

    public static Date dateFormat2(Date date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(format.format(date));
    }
}
