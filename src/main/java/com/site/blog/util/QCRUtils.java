package com.site.blog.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.site.blog.constants.UploadConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * @author YZD
 * 二维码生成器
 */
public class QCRUtils {

    private static final Integer WIDTH = 300;

    private static final Integer HEIGHT = 300;
    private static final Logger logger = LoggerFactory.getLogger(QCRUtils.class);

    public static void main(String[] args) throws IOException {
        generated(UploadConstants.QCR_IMAGES, "www.baidu.com");
    }


    public static void generated(String filePath, String content) throws IOException {
        // 文件存储实际路径
        File fileDirectory = new File(filePath);
        if (!fileDirectory.exists()) {
            if (!fileDirectory.mkdirs()) {
                logger.info("文件夹创建失败,路径为：" + fileDirectory);
                throw new IOException("文件夹创建失败,路径为：" + fileDirectory);
            }
        }
        Path path = Paths.get(filePath);
        String PNG = filePath.substring(filePath.lastIndexOf(".") + 1);
        // 定义二维码的配置，使用HashMap
        HashMap hints = new HashMap(4);
        // 字符集，内容使用的编码
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        // 容错等级，H、L、M、Q
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        // 边距，二维码距离边的空白宽度
        hints.put(EncodeHintType.MARGIN, 2);

        try {
            // 生成二维码对象，传入参数：内容、码的类型、宽高、配置
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints);
            // 生成二维码，传入二维码对象、生成图片的格式、生成的路径
            MatrixToImageWriter.writeToPath(bitMatrix, PNG, path);
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }

    }
}
