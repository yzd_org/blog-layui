package com.site.blog.util;

/**
 * @author YZD
 * @Descriotion 数字转汉字
 */
public class NumberToChineseUtils {

    private static final char[] RMB_NUMS = "零一二三四五六七八九".toCharArray();
    private static final String[] U1 = {"", "十", "百", "千"};
    private static final String[] U2 = {"", "万", "亿"};

    public static String integer2chinese(Integer integer) {
        String str = integer2chinese(String.valueOf(integer));
        if (integer > 9 && integer < 20) {
            str = str.substring(1);
        }
        return str;
    }

    public static String integer2chinese(String integer) {
        StringBuilder buffer = new StringBuilder();
        // 从个位数开始转换
        int i, j;
        for (i = integer.length() - 1, j = 0; i >= 0; i--, j++) {
            char n = integer.charAt(i);
            if (n == '0') {
                // 当n是0且n的右边一位不是0时，插入[零]
                if (i < integer.length() - 1 && integer.charAt(i + 1) != '0') {
                    buffer.append(RMB_NUMS[0]);
                }
                // 插入[万]或者[亿]
                if (j % 4 == 0) {
                    if (i > 0 && integer.charAt(i - 1) != '0'
                            || i > 1 && integer.charAt(i - 2) != '0'
                            || i > 2 && integer.charAt(i - 3) != '0') {
                        buffer.append(U2[j / 4]);
                    }
                }
            } else {
                if (j % 4 == 0) {
                    // 插入[万]或者[亿]
                    buffer.append(U2[j / 4]);
                }
                // 插入[拾]、[佰]或[仟]
                buffer.append(U1[j % 4]);
                // 插入数字
                buffer.append(RMB_NUMS[n - '0']);
            }
        }
        return buffer.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(integer2chinese(11));
    }


}
