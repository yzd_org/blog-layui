package com.site.blog.util;

/**
 * @author YZD
 * 分页参数验证
 * @date 2020/3/24
 */
public class PageParamUtils {

    /**
     * 分页参数验证
     *
     * @param pageNum  页码
     * @param pageSize 每页数量
     * @return boolean
     * @author YZD
     */
    public static boolean isValid(Integer pageNum, Integer pageSize) {
        boolean illegal = (pageNum == null || pageSize == null || pageNum.intValue() <= 0 || pageSize.intValue() <= 0) || pageSize.intValue() > 20;
        return !illegal;
    }
}
