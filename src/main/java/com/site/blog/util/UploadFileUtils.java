package com.site.blog.util;

import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @classname: UploadFileUtils
 * @description: 上传文件工具类
 * @author: 阳沐之
 * @create: 2020-03-24 15:24
 **/
public class UploadFileUtils {

    /**
     * 获取图片后缀
     */
    public static String getSuffixName(MultipartFile file){
        String fileName = file.getOriginalFilename();
        return fileName.substring(fileName.lastIndexOf("."));
    }
    
    /**
     * 生成文件名称通用方法
     */
    public static String getNewFileName(String suffixName){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        int random = new Random().nextInt(100);
        StringBuilder tempName = new StringBuilder();
        tempName.append(sdf.format(new Date())).append(random).append(suffixName);
        return tempName.toString();
    }

    /**
     * 默认文件名  自定义上传
     */
    public static String getFileName(MultipartFile file) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String suffixName = file.getOriginalFilename();
        StringBuilder tempName = new StringBuilder();
        tempName.append(sdf.format(new Date()) + "_").append(suffixName);
        return tempName.toString();
    }
}
