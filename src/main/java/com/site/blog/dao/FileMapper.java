package com.site.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.site.blog.entity.BlogFile;

/**
 * @Description: 文件
 * @author: YZD
 * @Date: 2020-03-07 15:24
 * @Version: 1.0
 */
public interface FileMapper extends BaseMapper<BlogFile> {
}
