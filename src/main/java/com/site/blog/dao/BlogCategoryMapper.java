package com.site.blog.dao;

import com.site.blog.entity.BlogCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 博客分类 Mapper 接口
 */
public interface BlogCategoryMapper extends BaseMapper<BlogCategory> {

}
