package com.site.blog.dao;

import com.site.blog.entity.BlogLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yzd
 * @since 2020-03-07
 */
public interface TbBlogLogMapper extends BaseMapper<BlogLog> {

}
