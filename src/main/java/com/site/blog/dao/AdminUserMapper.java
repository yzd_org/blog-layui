package com.site.blog.dao;

import com.site.blog.entity.AdminUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 后台管理员信息表 Mapper 接口
 */
public interface AdminUserMapper extends BaseMapper<AdminUser> {

}
