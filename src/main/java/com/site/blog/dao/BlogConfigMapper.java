package com.site.blog.dao;

import com.site.blog.entity.BlogConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper 接口
 */
public interface BlogConfigMapper extends BaseMapper<BlogConfig> {

}
