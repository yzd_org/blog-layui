package com.site.blog.dao;

import com.site.blog.entity.BlogTagRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 博客跟标签的关系表 Mapper 接口
 */
public interface BlogTagRelationMapper extends BaseMapper<BlogTagRelation> {

}
