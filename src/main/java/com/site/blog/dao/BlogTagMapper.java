package com.site.blog.dao;

import com.site.blog.entity.BlogTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 标签表 Mapper 接口
 */
public interface BlogTagMapper extends BaseMapper<BlogTag> {

}
