package com.site.blog.dao;

import com.site.blog.entity.BlogLink;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**

 * 友情链接表 Mapper 接口

 */
public interface BlogLinkMapper extends BaseMapper<BlogLink> {

}
