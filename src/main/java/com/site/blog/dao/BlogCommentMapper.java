package com.site.blog.dao;

import com.site.blog.entity.BlogComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 评论信息表 Mapper 接口
 */
public interface BlogCommentMapper extends BaseMapper<BlogComment> {

}
