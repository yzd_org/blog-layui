package com.site.blog.dao;

import com.site.blog.entity.BlogInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 博客信息表 Mapper 接口
 */
public interface BlogInfoMapper extends BaseMapper<BlogInfo> {

}
