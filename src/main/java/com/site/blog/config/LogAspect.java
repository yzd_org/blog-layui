package com.site.blog.config;

import com.site.blog.constants.LogConstants;
import com.site.blog.constants.SessionConstants;
import com.site.blog.entity.BlogLog;
import com.site.blog.service.BlogLogService;
import com.site.blog.util.IPUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;

/**
 * 日志切面
 *
 * @author yzd
 */
@Aspect
@Component
public class LogAspect {

    @Autowired
    private BlogLogService blogLogService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Pointcut("execution(public * com.site.blog.controller.blog.*.*(..))")
    public void webLog() {
    }

    //游客访问记录
    @Before("execution(public * com.site.blog.controller.blog.MyBlogController.index(..))")
    public void doVisit(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        String remoteAddr = IPUtil.getIpAddr(request);
        logger.info("URL : " + request.getRequestURL().toString());
        logger.info("HTTP_METHOD : " + request.getMethod());
        logger.info("IP : " + remoteAddr);
        logger.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));

        BlogLog tbBlogLog = new BlogLog().setDate(new Date()).setCount(1).setDescription(LogConstants.visit.getDescription()).setIp(remoteAddr);
        blogLogService.save(tbBlogLog);
    }

    //登出记录
    @Before("execution(public * com.site.blog.controller.admin.AdminController.logout(..))")
    public void doLogout(JoinPoint joinPoint) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String remoteAddr = IPUtil.getIpAddr(request);
        int userId = (int) request.getSession().getAttribute(SessionConstants.LOGIN_USER_ID);

        BlogLog tbBlogLog = new BlogLog().setUserId(userId).setDate(new Date()).setCount(1).setDescription(LogConstants.logout.getDescription()).setIp(remoteAddr);
        blogLogService.save(tbBlogLog);
    }

    @After("execution(public * com.site.blog.controller.admin.AdminController.login(..))")
    public void doLogin(JoinPoint joinPoint) throws Throwable {

    }

    @AfterReturning(returning = "ret", pointcut = "webLog()")
    public void doAfterReturning(Object ret) throws Throwable {
        logger.info("处理完请求，返回内容 : " + ret);
    }

    //后置异常通知
    @AfterThrowing("webLog()")
    public void throwss(JoinPoint jp) {
    }

    //后置最终通知,final增强，不管是抛出异常或者正常退出都会执行
    @After("webLog()")
    public void after(JoinPoint jp) {
    }

    //环绕通知,环绕增强，相当于MethodInterceptor
    @Around("webLog()")
    public Object arround(ProceedingJoinPoint pjp) throws Throwable {
        try {
            Object o = pjp.proceed();
            return o;
        } catch (Throwable e) {
            throw e;
        }
    }
}