package com.site.blog.config;

import com.site.blog.constants.HttpStatusConstants;
import com.site.blog.dto.Result;
import com.site.blog.util.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 *  全局异常统一处理类
 * @author yzd
 * */
@ControllerAdvice
public class MyControllerExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * 方法参数绑定 效验
     */
    @ExceptionHandler(value = BindException.class)
    @ResponseBody
    public Result BindException(BindException e) {
        return ResponseEntity.byStatusAndData(HttpStatusConstants.BAD_REQUEST, Objects.requireNonNull(e.getBindingResult().getFieldError()).getDefaultMessage());
    }


    /**
     * 方法参数无效 效验
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public Result handlerMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return ResponseEntity.byStatusAndData(HttpStatusConstants.BAD_REQUEST, Objects.requireNonNull(e.getBindingResult().getFieldError()).getDefaultMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handlerError(HttpServletRequest req, Exception e, HttpServletResponse httpServletResponse) throws Exception {
        logger.error("requestURI: {}, error: {}" + req.getRequestURI(), e);
        httpServletResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return ResponseEntity.failMessage("出现异常错误,请及时查看后台日志！");
    }


}
