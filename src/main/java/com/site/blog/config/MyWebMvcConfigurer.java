package com.site.blog.config;

import com.site.blog.constants.UploadConstants;
import com.site.blog.interceptor.AdminLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 阳沐之
 */
@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {

    @Autowired
    private AdminLoginInterceptor adminLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加一个拦截器，拦截以/admin为前缀的url路径
        registry.addInterceptor(adminLoginInterceptor)
                .addPathPatterns("/admin/**")
                .excludePathPatterns("/admin/v1/login")
                .excludePathPatterns("/admin/v1/reload")
                .excludePathPatterns("/admin/dist/**")
                .excludePathPatterns("/admin/plugins/**")
                .excludePathPatterns("/X-admin/**");
    }

    //     重写addResourceHandlers映射文件路径
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //图片
        registry.addResourceHandler("/authorImg/**").addResourceLocations("file:" + UploadConstants.UPLOAD_AUTHOR_IMG);
        //文章图片、文件
        registry.addResourceHandler("/upload/**").addResourceLocations("file:" + UploadConstants.FILE_UPLOAD_DIC);
        // 系统二维码
        registry.addResourceHandler("/myblog/**").addResourceLocations("file:" + UploadConstants.QCR_IMAGES);
        //自定义文件上传路径
        registry.addResourceHandler("/myfile/**").addResourceLocations("file:" + UploadConstants.UPLOAD);
        //系统日志
        registry.addResourceHandler("/log/**").addResourceLocations("file:" + UploadConstants.SYSLOG);
    }
}
