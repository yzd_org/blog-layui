package com.site.blog;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.site.blog.constants.FileConstants;
import com.site.blog.constants.UploadConstants;
import com.site.blog.entity.BlogConfig;
import com.site.blog.entity.BlogFile;
import com.site.blog.service.BlogConfigService;
import com.site.blog.service.BlogFileService;
import com.site.blog.util.QCRUtils;
import com.site.blog.util.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * @author yzd
 */
@Component
public class MyBlogRunner implements CommandLineRunner {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BlogFileService blogFileService;

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private BlogConfigService blogConfigService;

    private final static String path = UploadConstants.QCR_IMAGES;

    @Override
    public void run(String... args) throws IOException {
        logger.info("服务器启动后------->更新二维码，执行...");
//        String url = serverConfig.getUrl();
        String url = "http://122.51.85.120:8080/blog";
//        String url = "http://www.yibin.pub/blog";
        String URL = url + "/myblog/QCR.png";
        QCRUtils.generated(path, url);
        BlogFile blogFile = new BlogFile().setUrl(URL).setName(FileConstants.qcr.getDescription()).setType(FileConstants.qcr.getType()).setUpdateTime(new Date());
        BlogConfig blogConfig = new BlogConfig().setConfigField("qcr").setConfigName("系统二维码").setConfigValue(URL).setUpdateTime(new Date());
        blogConfigService.update(blogConfig, new QueryWrapper<BlogConfig>().lambda().eq(BlogConfig::getConfigField, blogConfig.getConfigField()));
        blogFileService.update(blogFile, new UpdateWrapper<BlogFile>().lambda().eq(BlogFile::getType, FileConstants.qcr.getType()));
    }
}
