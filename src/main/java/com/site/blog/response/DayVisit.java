package com.site.blog.response;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @author: YZD
 * @Date: 2020-04-08 21:17
 * @Version: 1.0
 */
@Data
@ApiModel(description = "系统统计中间实体")
public class DayVisit {
    Date date;
}
